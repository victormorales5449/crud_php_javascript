-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-04-2023 a las 14:45:23
-- Versión del servidor: 10.4.27-MariaDB
-- Versión de PHP: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `blog`
--
CREATE DATABASE IF NOT EXISTS `blog` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `blog`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articles`
--

CREATE TABLE `articles` (
  `article_id` bigint(20) UNSIGNED NOT NULL,
  `id_category` bigint(20) UNSIGNED NOT NULL,
  `id_author` bigint(20) UNSIGNED NOT NULL,
  `article_title` varchar(255) NOT NULL,
  `article_content` text NOT NULL,
  `article_image` varchar(255) NOT NULL,
  `article_created_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Truncar tablas antes de insertar `articles`
--

TRUNCATE TABLE `articles`;
--
-- Volcado de datos para la tabla `articles`
--

INSERT INTO `articles` (`article_id`, `id_category`, `id_author`, `article_title`, `article_content`, `article_image`, `article_created_time`) VALUES
(6, 16, 2, 'Learn git concepts, not commands', '<p>An interactive git tutorial meant to teach you how git works, not just which commands to execute.</p>\r\n\r\n<h1>Overview</h1>\r\n\r\n<p>In the picture below you see four boxes. One of them stands alone, while the other three are grouped together in what I&#39;ll call your Development Environment.</p>\r\n\r\n<p>We&#39;ll start with the one that&#39;s on it&#39;s own though. The Remote Repository is where you send your changes when you want to share them with other people, and where you get their changes from. If you&#39;ve used other version control systems there&#39;s nothing interesting about that.</p>\r\n\r\n<p>The Development Environment is what you have on your local machine. The three parts of it are your Working Directory, the Staging Area and the Local Repository. We&#39;ll learn more about those as we start using git.</p>\r\n\r\n<p>Choose a place in which you want to put your Development Environment. Just go to your home folder, or where ever you like to put your projects. You don&#39;t need to create a new folder for your Dev Environment though.</p>\r\n\r\n<h1>Getting a Remote Repository</h1>\r\n\r\n<p>Now we want to grab a Remote Repository and put what&#39;s in it onto your machine.</p>\r\n\r\n<p>Now that you have a copy of my Remote Repository of your own, it&#39;s time to get that onto your machine.</p>\r\n', 'git.png', '2020-02-14 10:28:00'),
(17, 4, 1, '10 Data Science and Machine Learning Courses for Beginners ', '<p>Data Science, Machine Learning, Deep Learning, and Artificial intelligence are really hot at this moment and offering a lucrative career to programmers with high pay and exciting work. It&#39;s a great opportunity for programmers who are willing to learn these new skills and upgrade themselves and want to solve some of the most interesting real-world problems. It&#39;s also important from the job perspective because Robots and Bots are getting smarter day by day, thanks to these technologies and most likely will take over some of the jobs which many programmers do today. Hence, it&#39;s important for software engineers and developers to upgrade themselves with these skills. Programmers with these skills are also commanding significantly higher salaries as data science is revolutionizing the world around us.</p>\r\n', 'datascience.jpg', '2020-02-14 11:52:24'),
(26, 2, 2, 'Anybody wants to start/practice contributing to OSS projects?? ', '<h1>Introduction</h1>\r\n\r\n<p>I made this super simple ruby gem called <a href=\"https://github.com/K-Sato1995/spell_generator\">spell_generator</a> and looking for people who want to start/practice contributing to OSS projects.</p>\r\n\r\n<h1>Why?</h1>\r\n\r\n<p>Making a PR to an OSS project can be intimidating if you don&#39;t know how to actually do to it. But if you know the process of making a PR and have already done it before, it can be very simple and fun.<br />\r\nI want to help people who have been thinking about contributing to OSS projects but have been too scared to do it.<br />\r\nEspecially if you are a self-taught programmer or new graduate who is looking for some experience to put on your resume, contributions to OSS projects would be great to have as your experience.</p>\r\n\r\n<p><strong>I want to help you to take the first step of your OSS journey.</strong></p>\r\n\r\n<h1>What I want you to do</h1>\r\n\r\n<p>As I mentioned above, this gem I created is very simple.<br />\r\nAll it does is creating a random spell based on adjectives and verbs that are stored in different arrays respectively.</p>\r\n\r\n<p>I want you to add one or more adjectives or verbs to the corresponding array.</p>\r\n\r\n<p><a href=\"https://github.com/K-Sato1995/spell_generator/issues/1\">https://github.com/K-Sato1995/spell_generator/issues/1</a></p>\r\n', '6.jpg', '2020-02-14 15:05:13'),
(27, 1, 1, 'Create your YOUR Personal Blog 📜 using Reactjs ⚛️ & Github Issues in less than 10 min  ', '<h1>React Blog</h1>\r\n\r\n<h2>React + Github Issues 👉 Your Personal Blog 🔥</h2>\r\n\r\n<p>React Blog is a personal blog system build on React that helps you create your own personal blog using Github Issues</p>\r\n\r\n<p>Link : <a href=\"https://github.com/saadpasta/react-blog-github\">https://github.com/saadpasta/react-blog-github</a></p>\r\n\r\n<h2>🔥 Features</h2>\r\n\r\n<p>✅ Own your content<br />\r\n✅ Write using Markdown On Github Issues<br />\r\n✅ Syntax/Code Highlighting<br />\r\n✅ Fully customizable<br />\r\n✅ Tags - Topics<br />\r\n<br />\r\n✅ Links<br />\r\n✅ Reaction<br />\r\n✅ Images<br />\r\n✅ Minutes Read<br />\r\n✅ Beautiful UI Like Medium<br />\r\n✅ Easy deployment: Using Github Pages<br />\r\n✅ Beautiful blockquote</p>\r\n\r\n<h2>🔗 Live Demo</h2>\r\n\r\n<p>Here&#39;s a <a href=\"https://saadpasta.github.io/react-blog-github/#/\">live demo</a></p>\r\n\r\n<p>Github <a href=\"https://github.com/saadpasta/react-blog-github/issues\">Issues / Blogs</a></p>\r\n\r\n<hr />\r\n<h2>🚀 Get Up and Running in 10 Minutes</h2>\r\n\r\n<p>You can get a react-blog site up and running on your local dev environment in 10 minutes with these five steps:</p>\r\n\r\n<p>These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.</p>\r\n', 'bbgv69gnhmi97nihyc6k.webp', '2020-02-14 15:55:01'),
(40, 14, 8, 'Role-based auth in Angular 9 🔑 ', '<p>You are welcome to join LIVE MasterClass 📺 18-02-2020 8 pm GMT</p>\r\n\r\n<p>Free LIVE MasterClass about designing a role-based system in Angular 🅰</p>\r\n\r\n<p>Register 👉 <a href=\"https://mailchi.mp/9038cbea0f9c/role-based-auth-angular\">https://mailchi.mp/9038cbea0f9c/role-based-auth-angular</a></p>\r\n\r\n<p>⚡ the case of role-based application<br />\r\n⚡ a domain model for a multi-account system<br />\r\n⚡ vectors of authorization<br />\r\n⚡ designing secure REST API for roles<br />\r\n⚡ userAuth object<br />\r\n⚡ Router Guards<br />\r\n⚡ HttpInterceptors<br />\r\n⚡ Silent auth (RxJS magic!)<br />\r\n⚡ Express.js middlewares<br />\r\n⚡ DEMO</p>\r\n', 'xghawvmrh6v3z4pmvgts.webp', '2020-02-18 12:00:01'),
(41, 1, 1, 'The 25 most recommended programming books of all-time', '<h2>Methodology:</h2>\r\n\r\n<p>I&#39;ve simply asked Google for a few queries like &quot;Best Programming Books&quot; and its variations. I have then scrapped all those pages (using ScrapingBee, a web scraping API I&#39;m working on).</p>\r\n\r\n<p>I&#39;ve deduplicated the links and ended up with nearly 150 links. Using the title of the pages I was also able to quickly discards:</p>\r\n\r\n<ul>\r\n	<li>list focussed on one particular technology or platform</li>\r\n	<li>list focussed on one particular year</li>\r\n	<li>list focussed on free books</li>\r\n	<li>Quora and Reddit threads</li>\r\n</ul>\r\n\r\n<p>I ended up with almost 200 HTML files. I went on opening all the files on my browser, open my chrome inspector, found and wrote the CSS selector matching book titles in the article. This took me around 1hours, almost 30 seconds per page.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>25. <a href=\"https://amzn.to/2OZ9JXS\">Continuous Delivery</a> by Jez Humble &amp; David Farley (8.8% recommended)</h3>\r\n\r\n<p><a href=\"https://res.cloudinary.com/practicaldev/image/fetch/s--EQoynaP---/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/25.jpg%23center\"><img alt=\"\" src=\"https://res.cloudinary.com/practicaldev/image/fetch/s--EQoynaP---/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/25.jpg%23center\" /></a></p>\r\n\r\n<p>&quot;Getting software released to users is often a painful, risky, and time-consuming process. This groundbreaking new book sets out the principles and technical practices that enable rapid, incremental delivery of high quality, valuable new functionality to users. Through automation of the build, deployment, and testing process, and improved collaboration between developers, testers, and operations, delivery teams can get changes released in a matter of hours, sometimes even minutes&ndash;no matter what the size of a project or the complexity of its code base.</p>\r\n\r\n<p>Jez Humble and David Farley begin by presenting the foundations of a rapid, reliable, low-risk delivery process. Next, they introduce the &ldquo;deployment pipeline,&rdquo; an automated process for managing all changes, from check-in to release. Finally, they discuss the &ldquo;ecosystem&rdquo; needed to<br />\r\nsupport continuous delivery, from infrastructure, data and configuration management to governance.&quot; <a href=\"https://amzn.to/2OZ9JXS\">Amazon.com</a></p>\r\n\r\n<h3>24. <a href=\"https://amzn.to/2u2vpuG\">Algorithms</a> by Robert Sedgewick &amp; Kevin Wayne (8.8% recommended)</h3>\r\n\r\n<p><a href=\"https://res.cloudinary.com/practicaldev/image/fetch/s--QEgOyA1_--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/24.jpg%23center\"><img alt=\"\" src=\"https://res.cloudinary.com/practicaldev/image/fetch/s--QEgOyA1_--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/24.jpg%23center\" /></a><br />\r\n&quot;The algorithms in this book represent a body of knowledge developed over the last 50 years that has become indispensable, not just for professional programmers and computer science students but for any student with interests in science, mathematics, and engineering, not to mention students who use computation in the liberal arts.&quot;<a href=\"https://amzn.to/2u2vpuG\">Amazon.com</a></p>\r\n\r\n<h3>23. <a href=\"https://amzn.to/325cN9T\">The Self-Taught Programmer</a> by Cory Althoff (8.8% recommended)</h3>\r\n\r\n<p><a href=\"https://res.cloudinary.com/practicaldev/image/fetch/s--UBDs1gW6--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/23.jpg%23center\"><img alt=\"\" src=\"https://res.cloudinary.com/practicaldev/image/fetch/s--UBDs1gW6--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/23.jpg%23center\" /></a><br />\r\n&quot;I am a self-taught programmer. After a year of self-study, I learned to program well enough to land a job as a software engineer II at eBay.</p>\r\n\r\n<p>Once I got there, I realized I was severely under-prepared. I was overwhelmed by the amount of things I needed to know but hadn&#39;t learned yet. My journey learning to program, and my experience at my first job as a software engineer were the inspiration for this book.</p>\r\n\r\n<p>This book is not just about learning to program; although you will learn to code. If you want to program professionally, it is not enough to learn to code; that is why, in addition to helping you learn to program, I also cover the rest of the things you need to know to program professionally that classes and books don&#39;t teach you.</p>\r\n\r\n<p>&quot;The Self-taught Programmer&quot; is a roadmap, a guide to take you from writing your first Python program, to passing your first technical interview. The path is there. Will you take it?&quot;<a href=\"https://amzn.to/325cN9T\">Amazon.com</a></p>\r\n\r\n<h3>22. <a href=\"https://amzn.to/2SQLQTn\">Rapid Development</a> by Steve McConnell (8.8% recommended)</h3>\r\n\r\n<p><a href=\"https://res.cloudinary.com/practicaldev/image/fetch/s--xenhZVHv--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/22.jpg%23center\"><img alt=\"\" src=\"https://res.cloudinary.com/practicaldev/image/fetch/s--xenhZVHv--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/22.jpg%23center\" /></a><br />\r\n&quot;Corporate and commercial software-development teams all want solutions for one important problem&mdash;how to get their high-pressure development schedules under control. In RAPID DEVELOPMENT, author Steve McConnell addresses that concern head-on with overall strategies, specific best practices, and valuable tips that help shrink and control development schedules and keep projects moving. Inside, you&rsquo;ll find:</p>\r\n\r\n<ul>\r\n	<li>A rapid-development strategy that can be applied to any project and the best practices to make that strategy work</li>\r\n	<li>Candid discussions of great and not-so-great rapid-development practices&mdash;estimation, prototyping, forced overtime, motivation, teamwork, rapid-development languages, risk management, and many others</li>\r\n	<li>A list of classic mistakes to avoid for rapid-development projects, including creeping requirements, shortchanged quality, and silver-bullet syndrome</li>\r\n	<li>Case studies that vividly illustrate what can go wrong, what can go right, and how to tell which direction your project is going</li>\r\n	<li>RAPID DEVELOPMENT is the real-world guide to more efficient applications development.&quot;<a href=\"https://amzn.to/2SQLQTn\">Amazon.com</a></li>\r\n</ul>\r\n\r\n<h3>21. <a href=\"https://amzn.to/323c5Ki\">Coders at Work</a> by Peter Seibel (10.2% recommended)</h3>\r\n\r\n<p><a href=\"https://res.cloudinary.com/practicaldev/image/fetch/s--65YlM020--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/21.jpg%23center\"><img alt=\"\" src=\"https://res.cloudinary.com/practicaldev/image/fetch/s--65YlM020--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/21.jpg%23center\" /></a><br />\r\n&quot;This is a who&#39;s who in the programming world - a fascinating look at how some of the best in the world do their work. Patterned after the best selling Founders at Work, the book represents two years of interviews with some of the top programmers of our times.&quot;<a href=\"https://amzn.to/323c5Ki\">Amazon.com</a></p>\r\n\r\n<h3>20. <a href=\"https://amzn.to/38BFCxd\">Domain-Driven Design</a> by Eric Evans (10.2% recommended)</h3>\r\n\r\n<p><a href=\"https://res.cloudinary.com/practicaldev/image/fetch/s--atepd61T--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/20.jpg%23center\"><img alt=\"\" src=\"https://res.cloudinary.com/practicaldev/image/fetch/s--atepd61T--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/20.jpg%23center\" /></a><br />\r\n&quot;Leading software designers have recognized domain modeling and design as critical topics for at least twenty years, yet surprisingly little has been written about what needs to be done or how to do it. Although it has never been clearly formulated, a philosophy has developed as an undercurrent in the object community, which I call &quot;domain-driven design&quot;.</p>\r\n\r\n<p>I have spent the past decade focused on developing complex systems in several business and technical domains. I&#39;ve tried best practices in design and development process as they have emerged from the leaders in the object-oriented development community. Some of my projects were very successful; a few failed. A feature common to the successes was a rich domain model that evolved through iterations of design and became part of the fabric of the project.</p>\r\n\r\n<p>This book provides a framework for making design decisions and a technical vocabulary for discussing domain design. It is a synthesis of widely accepted best practices along with my own insights and experiences. Projects facing complex domains can use this framework to approach domain-driven design systematically.&quot;<a href=\"https://amzn.to/38BFCxd\">Amazon.com</a></p>\r\n\r\n<h3>19. <a href=\"https://amzn.to/2SU7N3Q\">The Art of Computer Programming</a> by Donald E. Knuth(10.2% recommended)</h3>\r\n\r\n<p><a href=\"https://res.cloudinary.com/practicaldev/image/fetch/s--0WI0lqmq--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/19.jpg%23center\"><img alt=\"\" src=\"https://res.cloudinary.com/practicaldev/image/fetch/s--0WI0lqmq--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/19.jpg%23center\" /></a><br />\r\n&quot;Countless readers have spoken about the profound personal influence of Knuth&rsquo;s work. Scientists have marveled at the beauty and elegance of his analysis, while ordinary programmers have successfully applied his &ldquo;cookbook&rdquo; solutions to their day-to-day problems. All have admired Knuth for the breadth, clarity, accuracy, and good humor found in his books.&quot;<a href=\"https://amzn.to/2SU7N3Q\">Amazon.com</a></p>\r\n\r\n<h3>18. <a href=\"https://amzn.to/2HJ7HqR\">Structure and Interpretation of Computer Programs</a> by Harold Abelson / Gerald Jay Sussman / Julie Sussman (13.2% recommended)</h3>\r\n\r\n<p><a href=\"https://res.cloudinary.com/practicaldev/image/fetch/s--NcbSb2NW--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/17.jpg%23center\"><img alt=\"\" src=\"https://res.cloudinary.com/practicaldev/image/fetch/s--NcbSb2NW--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/17.jpg%23center\" /></a><br />\r\n&quot;Compilers: Principles, Techniques and Tools, known to professors, students, and developers worldwide as the &quot;Dragon Book,&quot; is available in a new edition. Every chapter has been completely revised to reflect developments in software engineering, programming languages, and computer architecture that have occurred since 1986, when the last edition published. The authors, recognizing that few readers will ever go on to construct a compiler, retain their focus on the broader set of problems faced in software design and software development.&quot;<a href=\"https://amzn.to/2HJ7HqR\">Amazon.com</a></p>\r\n\r\n<h3>17. <a href=\"https://amzn.to/37xEadR\">Patterns of Enterprise Application Architecture</a> by Martin Fowler (14.7% recommended)</h3>\r\n\r\n<p><a href=\"https://res.cloudinary.com/practicaldev/image/fetch/s--Sgio7eNO--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/16.jpg%23center\"><img alt=\"\" src=\"https://res.cloudinary.com/practicaldev/image/fetch/s--Sgio7eNO--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/16.jpg%23center\" /></a><br />\r\n&quot;The practice of enterprise application development has benefited from the emergence of many new enabling technologies. Multi-tiered object-oriented platforms, such as Java and .NET, have become commonplace. These new tools and technologies are capable of building powerful applications, but they are not easily implemented. Common failures in enterprise applications often occur because their developers do not understand the architectural lessons that experienced object developers have learned.&quot;<a href=\"https://amzn.to/37xEadR\">Amazon.com</a></p>\r\n\r\n<h3>16. <a href=\"https://amzn.to/2u8cOxo\">Programming Pearls</a> by Jon Bentley (16.1% recommended)</h3>\r\n\r\n<p><a href=\"https://res.cloudinary.com/practicaldev/image/fetch/s--7xgqyQ44--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/16-bis.jpg%23center\"><img alt=\"\" src=\"https://res.cloudinary.com/practicaldev/image/fetch/s--7xgqyQ44--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/16-bis.jpg%23center\" /></a><br />\r\n&quot;Computer programming has many faces. Fred Brooks paints the big picture in The Mythical Man Month; his essays underscore the crucial role of management in large software projects. At a finer grain, Steve McConnell teaches good programming style in Code Complete. The topics in those books are the key to good software and the hallmark of the professional programmer. Unfortunately, though, the workmanlike application of those sound engineering principles isn&#39;t always thrilling -- until the software is completed on time and works without surprise.</p>\r\n\r\n<p>The columns in this book are about a more glamorous aspect of the profession: programming pearls whose origins lie beyond solid engineering, in the realm of insight and creativity. Just as natural pearls grow from grains of sand that have irritated oysters, these programming pearls have grown from real problems that have irritated real programmers. The programs are fun, and they teach important programming techniques and fundamental design principles.&quot;<a href=\"https://amzn.to/2u8cOxo\">Amazon.com</a></p>\r\n\r\n<h3>15. <a href=\"https://amzn.to/2P2YRIh\">Peopleware</a> by Tom DeMarco &amp; Tim Lister (17.6% recommended)</h3>\r\n\r\n<p><a href=\"https://res.cloudinary.com/practicaldev/image/fetch/s--3TCF5DDY--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/15.jpg%23center\"><img alt=\"\" src=\"https://res.cloudinary.com/practicaldev/image/fetch/s--3TCF5DDY--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/15.jpg%23center\" /></a><br />\r\n&quot;Drawing on their software development and management experience, and highlighting the insights and wisdom of other successful managers, Mantle and Lichty provide the rules, tools, and insights you need to manage and understand people and teams in order to deliver software successfully and avoid projects that have run catastrophically over schedule and budget. The unique insight of this longtime bestseller is that the major issues of software development are human, not technical. They&#39;re not easy issues; but solve them, and you&#39;ll maximize your chances of success. With a blend of software engineering facts and thought-provoking opinions, Fred Brooks offers insight for anyone managing complex projects.&quot;<a href=\"https://amzn.to/2P2YRIh\">Amazon.com</a></p>\r\n\r\n<h3>14. <a href=\"https://amzn.to/2SAdPrt\">Introduction to Algorithms</a> by Thomas H. Cormen / Charles E. Leiserson / Ronald L. Rivest / Clifford Stein (17.6% recommended)</h3>\r\n\r\n<p><a href=\"https://res.cloudinary.com/practicaldev/image/fetch/s--bA90l1nK--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/14.jpg%23center\"><img alt=\"\" src=\"https://res.cloudinary.com/practicaldev/image/fetch/s--bA90l1nK--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/14.jpg%23center\" /></a><br />\r\n&quot;Some books on algorithms are rigorous but incomplete; others cover masses of material but lack rigor. Introduction to Algorithms uniquely combines rigor and comprehensiveness. The book covers a broad range of algorithms in depth, yet makes their design and analysis accessible to all levels of readers. Each chapter is relatively self-contained and can be used as a unit of study. The algorithms are described in English and in a pseudocode designed to be readable by anyone who has done a little programming. The explanations have been kept elementary without sacrificing depth of coverage or mathematical rigor.</p>\r\n\r\n<p>The first edition became a widely used text in universities worldwide as well as the standard reference for professionals. The second edition featured new chapters on the role of algorithms, probabilistic analysis and randomized algorithms, and linear programming. The third edition has been revised and updated throughout. It includes two completely new chapters, on van Emde Boas trees and multithreaded algorithms, substantial additions to the chapter on recurrence (now called &ldquo;Divide-and-Conquer&rdquo;), and an appendix on matrices. It features improved treatment of dynamic programming and greedy algorithms and a new notion of edge-based flow in the material on flow networks. Many exercises and problems have been added for this edition. The international paperback edition is no longer available; the hardcover is available worldwide.&quot;<a href=\"https://amzn.to/2SAdPrt\">Amazon.com</a></p>\r\n\r\n<h3>13. <a href=\"https://amzn.to/38zhTOo\">Code</a> by Charles Petzold (19.1% recommended)</h3>\r\n\r\n<p><a href=\"https://res.cloudinary.com/practicaldev/image/fetch/s--VLfMWdny--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/13.jpg%23center\"><img alt=\"\" src=\"https://res.cloudinary.com/practicaldev/image/fetch/s--VLfMWdny--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/13.jpg%23center\" /></a><br />\r\n&quot;What do flashlights, the British invasion, black cats, and seesaws have to do with computers? In CODE, they show us the ingenious ways we manipulate language and invent new means of communicating with each other. And through CODE, we see how this ingenuity and our very human compulsion to communicate have driven the technological innovations of the past two centuries.</p>\r\n\r\n<p>Using everyday objects and familiar language systems such as Braille and Morse code, author Charles Petzold weaves an illuminating narrative for anyone who&rsquo;s ever wondered about the secret inner life of computers and other smart machines.</p>\r\n\r\n<p>It&rsquo;s a cleverly illustrated and eminently comprehensible story&mdash;and along the way, you&rsquo;ll discover you&rsquo;ve gained a real context for understanding today&rsquo;s world of PCs, digital media, and the Internet. No matter what your level of technical savvy, CODE will charm you&mdash;and perhaps even awaken the technophile within.&quot;<a href=\"https://amzn.to/38zhTOo\">Amazon.com</a></p>\r\n\r\n<h3>12. <a href=\"https://amzn.to/2UYPsW3\">Don&#39;t Make Me Think</a> by Steve Krug (19.1% recommended)</h3>\r\n\r\n<p><a href=\"https://res.cloudinary.com/practicaldev/image/fetch/s--Pf1e3mIa--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/12.jpg%23center\"><img alt=\"\" src=\"https://res.cloudinary.com/practicaldev/image/fetch/s--Pf1e3mIa--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/12.jpg%23center\" /></a><br />\r\n&quot;Since Don&rsquo;t Make Me Think was first published in 2000, hundreds of thousands of Web designers and developers have relied on usability guru Steve Krug&rsquo;s guide to help them understand the principles of intuitive navigation and information design. Witty, commonsensical, and eminently practical, it&rsquo;s one of the best-loved and most recommended books on the subject.</p>\r\n\r\n<p>Now Steve returns with fresh perspective to reexamine the principles that made Don&rsquo;t Make Me Think a classic&ndash;with updated examples and a new chapter on mobile usability. And it&rsquo;s still short, profusely illustrated&hellip;and best of all&ndash;fun to read.</p>\r\n\r\n<p>If you&rsquo;ve read it before, you&rsquo;ll rediscover what made Don&rsquo;t Make Me Think so essential to Web designers and developers around the world. If you&rsquo;ve never read it, you&rsquo;ll see why so many people have said it should be required reading for anyone working on Web sites.&quot;<a href=\"https://amzn.to/2UYPsW3\">Amazon.com</a></p>\r\n\r\n<h3>11. <a href=\"https://amzn.to/2HziTGl\">Soft Skills</a> by John Sonmez (22% recommended)</h3>\r\n\r\n<p><a href=\"https://res.cloudinary.com/practicaldev/image/fetch/s--JY5fhUMe--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/11.jpg%23center\"><img alt=\"\" src=\"https://res.cloudinary.com/practicaldev/image/fetch/s--JY5fhUMe--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/11.jpg%23center\" /></a><br />\r\n&quot;For most software developers, coding is the fun part. The hard bits are dealing with clients, peers, and managers, staying productive, achieving financial security, keeping yourself in shape, and finding true love. This book is here to help.</p>\r\n\r\n<p>Soft Skills: The software developer&#39;s life manual is a guide to a well-rounded, satisfying life as a technology professional. In it, developer and life coach John Sonmez offers advice to developers on important &quot;soft&quot; subjects like career and productivity, personal finance and investing, and even fitness and relationships. Arranged as a collection of 71 short chapters, this fun-to-read book invites you to dip in wherever you like. A Taking Action section at the end of each chapter shows you how to get quick results. Soft Skills will help make you a better programmer, a more valuable employee, and a happier, healthier person.&quot;<a href=\"https://amzn.to/2HziTGl\">Amazon.com</a></p>\r\n\r\n<h3>10. <a href=\"https://amzn.to/37CuhvD\">Cracking the Coding Interview</a> by Gayle Laakmann McDowell (22% recommended)</h3>\r\n\r\n<p><a href=\"https://res.cloudinary.com/practicaldev/image/fetch/s--Ibw0I7hL--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/10.jpg%23center\"><img alt=\"\" src=\"https://res.cloudinary.com/practicaldev/image/fetch/s--Ibw0I7hL--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/10.jpg%23center\" /></a><br />\r\n&quot;I am not a recruiter. I am a software engineer. And as such, I know what it&#39;s like to be asked to whip up brilliant algorithms on the spot and then write flawless code on a whiteboard. I&#39;ve been through this as a candidate and as an interviewer.</p>\r\n\r\n<p>Cracking the Coding Interview, 6th Edition is here to help you through this process, teaching you what you need to know and enabling you to perform at your very best. I&#39;ve coached and interviewed hundreds of software engineers. The result is this book.</p>\r\n\r\n<p>Learn how to uncover the hints and hidden details in a question, discover how to break down a problem into manageable chunks, develop techniques to unstick yourself when stuck, learn (or re-learn) core computer science concepts, and practice on 189 interview questions and solutions.</p>\r\n\r\n<p>These interview questions are real; they are not pulled out of computer science textbooks. They reflect what&#39;s truly being asked at the top companies, so that you can be as prepared as possible. WHAT&#39;S INSIDE?</p>\r\n\r\n<ul>\r\n	<li>189 programming interview questions, ranging from the basics to the trickiest algorithm problems.</li>\r\n	<li>A walk-through of how to derive each solution, so that you can learn how to get there yourself.</li>\r\n	<li>Hints on how to solve each of the 189 questions, just like what you would get in a real interview.</li>\r\n	<li>Five proven strategies to tackle algorithm questions, so that you can solve questions you haven&#39;t seen.</li>\r\n	<li>Extensive coverage of essential topics, such as big O time, data structures, and core algorithms.</li>\r\n	<li>A behind the scenes look at how top companies like Google and Facebook hire developers.</li>\r\n	<li>Techniques to prepare for and ace the soft side of the interview: behavioral questions.</li>\r\n	<li>For interviewers and companies: details on what makes a good interview question and hiring process.&quot;<a href=\"https://amzn.to/37CuhvD\">Amazon.com</a></li>\r\n</ul>\r\n\r\n<h3>9. <a href=\"https://amzn.to/38Eb9yO\">Design Patterns</a> by by Erich Gamma / Richard Helm / Ralph Johnson / John Vlissides (25% recommended)</h3>\r\n\r\n<p><a href=\"https://res.cloudinary.com/practicaldev/image/fetch/s--XdxT82Xp--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/9.jpg%23center\"><img alt=\"\" src=\"https://res.cloudinary.com/practicaldev/image/fetch/s--XdxT82Xp--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/9.jpg%23center\" /></a><br />\r\n&quot;Capturing a wealth of experience about the design of object-oriented software, four top-notch designers present a catalog of simple and succinct solutions to commonly occurring design problems. Previously undocumented, these 23 patterns allow designers to create more flexible, elegant, and ultimately reusable designs without having to rediscover the design solutions themselves.</p>\r\n\r\n<p>The authors begin by describing what patterns are and how they can help you design object-oriented software. They then go on to systematically name, explain, evaluate, and catalog recurring designs in object-oriented systems. With Design Patterns as your guide, you will learn how these important patterns fit into the software development process, and how you can leverage them to solve your own design problems most efficiently.</p>\r\n\r\n<p>Each pattern describes the circumstances in which it is applicable, when it can be applied in view of other design constraints, and the consequences and trade-offs of using the pattern within a larger design. All patterns are compiled from real systems and are based on real-world examples. Each pattern also includes code that demonstrates how it may be implemented in object-oriented programming languages like C++ or Smalltalk.&quot;<a href=\"https://amzn.to/38Eb9yO\">Amazon.com</a></p>\r\n\r\n<h3>8. <a href=\"https://amzn.to/2P37DWJ\">Working Effectively with Legacy Code</a> by Michael Feathers (26.4% recommended)</h3>\r\n\r\n<p><a href=\"https://res.cloudinary.com/practicaldev/image/fetch/s---9YagzAR--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/8.jpg%23center\"><img alt=\"\" src=\"https://res.cloudinary.com/practicaldev/image/fetch/s---9YagzAR--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/8.jpg%23center\" /></a><br />\r\n&quot;In this book, Michael Feathers offers start-to-finish strategies for working more effectively with large, untested legacy code bases. This book draws on material Michael created for his own renowned Object Mentor seminars: techniques Michael has used in mentoring to help hundreds of developers, technical managers, and testers bring their legacy systems under control.<br />\r\nThis book also includes a catalog of twenty-four dependency-breaking techniques that help you work with program elements in isolation and make safer changes.&quot;<a href=\"https://amzn.to/2P37DWJ\">Amazon.com</a></p>\r\n\r\n<h3>7. <a href=\"https://amzn.to/2u63SIS\">The Clean Coder</a> by Robert Martin (27.9% recommended)</h3>\r\n\r\n<p><a href=\"https://res.cloudinary.com/practicaldev/image/fetch/s--ZtiA7PUa--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/7.jpg%23center\"><img alt=\"\" src=\"https://res.cloudinary.com/practicaldev/image/fetch/s--ZtiA7PUa--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/7.jpg%23center\" /></a><br />\r\n&quot;Programmers who endure and succeed amidst swirling uncertainty and nonstop pressure share a common attribute: They care deeply about the practice of creating software. They treat it as a craft. They are professionals.</p>\r\n\r\n<p>In The Clean Coder: A Code of Conduct for Professional Programmers, legendary software expert Robert C. Martin introduces the disciplines, techniques, tools, and practices of true software craftsmanship. This book is packed with practical advice&ndash;about everything from estimating and coding to refactoring and testing. It covers much more than technique: It is about attitude. Martin shows how to approach software development with honor, self-respect, and pride; work well and work clean; communicate and estimate faithfully; face difficult decisions with clarity and honesty; and understand that deep knowledge comes with a responsibility to act.</p>\r\n\r\n<p>Great software is something to marvel at: powerful, elegant, functional, a pleasure to work with as both a developer and as a user. Great software isn&rsquo;t written by machines. It is written by professionals with an unshakable commitment to craftsmanship. The Clean Coder will help you become one of them&ndash;and earn the pride and fulfillment that they alone possess.&quot;<a href=\"https://amzn.to/2u63SIS\">Amazon.com</a></p>\r\n\r\n<h3>6. <a href=\"https://amzn.to/3bL1lVz\">The Mythical Man-Month</a> by Frederick P. Brooks Jr (27.9% recommended)</h3>\r\n\r\n<p><a href=\"https://res.cloudinary.com/practicaldev/image/fetch/s--VOxo9OdI--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/6.jpg%23center\"><img alt=\"\" src=\"https://res.cloudinary.com/practicaldev/image/fetch/s--VOxo9OdI--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/6.jpg%23center\" /></a><br />\r\n&quot;Few books on software project management have been as influential and timeless as The Mythical Man-Month. With a blend of software engineering facts and thought-provoking opinions, Fred Brooks offers insight for anyone managing complex projects. These essays draw from his experience as project manager for the IBM System/360 computer family and then for OS/360, its massive software system. Now, 20 years after the initial publication of his book, Brooks has revisited his original ideas and added new thoughts and advice, both for readers already familiar with his work and for readers discovering it for the first time.&quot;<a href=\"https://amzn.to/3bL1lVz\">Amazon.com</a></p>\r\n\r\n<h3>5. <a href=\"https://amzn.to/2SUiUtA\">Head First Design Patterns</a> by Eric Freeman / Bert Bates / Kathy Sierra / Elisabeth Robson (29.4% recommended)</h3>\r\n\r\n<p><a href=\"https://res.cloudinary.com/practicaldev/image/fetch/s--mEZ0dQqV--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/5.jpg%23center\"><img alt=\"\" src=\"https://res.cloudinary.com/practicaldev/image/fetch/s--mEZ0dQqV--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/5.jpg%23center\" /></a><br />\r\n&quot;What&rsquo;s so special about design patterns?</p>\r\n\r\n<p>At any given moment, someone struggles with the same software design problems you have. And, chances are, someone else has already solved your problem. This edition of Head First Design Patterns&mdash;now updated for Java 8&mdash;shows you the tried-and-true, road-tested patterns used by developers to create functional, elegant, reusable, and flexible software. By the time you finish this book, you&rsquo;ll be able to take advantage of the best design practices and experiences of those who have fought the beast of software design and triumphed.</p>\r\n\r\n<p>What&rsquo;s so special about this book?</p>\r\n\r\n<p>We think your time is too valuable to spend struggling with new concepts. Using the latest research in cognitive science and learning theory to craft a multi-sensory learning experience, Head First Design Patterns uses a visually rich format designed for the way your brain works, not a text-heavy approach that puts you to sleep.&quot;<a href=\"https://amzn.to/2SUiUtA\">Amazon.com</a></p>\r\n\r\n<h3>4. <a href=\"https://amzn.to/2uSs167\">Refactoring</a> by Martin Fowler (35% recommended)</h3>\r\n\r\n<p><a href=\"https://res.cloudinary.com/practicaldev/image/fetch/s--3ppuefuL--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/4.jpg%23center\"><img alt=\"\" src=\"https://res.cloudinary.com/practicaldev/image/fetch/s--3ppuefuL--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/4.jpg%23center\" /></a><br />\r\n&quot;As the application of object technology--particularly the Java programming language--has become commonplace, a new problem has emerged to confront the software development community. Significant numbers of poorly designed programs have been created by less-experienced developers, resulting in applications that are inefficient and hard to maintain and extend. Increasingly, software system professionals are discovering just how difficult it is to work with these inherited, non-optimal applications.</p>\r\n\r\n<p>For several years, expert-level object programmers have employed a growing collection of techniques to improve the structural integrity and performance of such existing software programs. Referred to as refactoring, these practices have remained in the domain of experts because no attempt has been made to transcribe the lore into a form that all developers could use. . .until now. In Refactoring: Improving the Design of Existing Software, renowned object technology mentor Martin Fowler breaks new ground, demystifying these master practices and demonstrating how software practitioners can realize the significant benefits of this new process. With proper training a skilled system designer&quot; <a href=\"https://amzn.to/2uSs167\">Amazon.com</a></p>\r\n\r\n<h3>3. <a href=\"https://amzn.to/37wO0wR\">Code Complete</a> by Steve McConnell (42% recommended)</h3>\r\n\r\n<p><a href=\"https://res.cloudinary.com/practicaldev/image/fetch/s--bB3dykHA--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/3.jpg%23center\"><img alt=\"\" src=\"https://res.cloudinary.com/practicaldev/image/fetch/s--bB3dykHA--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/3.jpg%23center\" /></a><br />\r\n&quot;Widely considered one of the best practical guides to programming, Steve McConnell&rsquo;s original CODE COMPLETE has been helping developers write better software for more than a decade. Now this classic book has been fully updated and revised with leading-edge practices&mdash;and hundreds of new code samples&mdash;illustrating the art and science of software construction. Capturing the body of knowledge available from research, academia, and everyday commercial practice, McConnell synthesizes the most effective techniques and must-know principles into clear, pragmatic guidance. No matter what your experience level, development environment, or project size, this book will inform and stimulate your thinking&mdash;and help you build the highest quality code.&quot;<a href=\"https://amzn.to/37wO0wR\">Amazon.com</a></p>\r\n\r\n<h3>2. <a href=\"https://amzn.to/2Hy0n0U\">Clean Code</a> by Robert C. Martin (66% recommended)</h3>\r\n\r\n<p><a href=\"https://res.cloudinary.com/practicaldev/image/fetch/s--tcK22PgO--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/2.jpg%23center\"><img alt=\"\" src=\"https://res.cloudinary.com/practicaldev/image/fetch/s--tcK22PgO--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/2.jpg%23center\" /></a><br />\r\n&quot;Clean Code is divided into three parts. The first describes the principles, patterns, and practices of writing clean code. The second part consists of several case studies of increasing complexity. Each case study is an exercise in cleaning up code&mdash;of transforming a code base that has some problems into one that is sound and efficient. The third part is the payoff: a single chapter containing a list of heuristics and &ldquo;smells&rdquo; gathered while creating the case studies. The result is a knowledge base that describes the way we think when we write, read, and clean code.&quot;<a href=\"https://amzn.to/2Hy0n0U\">Amazon.com</a></p>\r\n\r\n<h3>1. <a href=\"https://amzn.to/3bO6Xyb\">The Pragmatic Programmer</a> by David Thomas &amp; Andrew Hunt (67% recommended)</h3>\r\n\r\n<p><a href=\"https://res.cloudinary.com/practicaldev/image/fetch/s--V-6_BN0z--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/1.jpg%23center\"><img alt=\"\" src=\"https://res.cloudinary.com/practicaldev/image/fetch/s--V-6_BN0z--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.daolf.com/images/programming_book_list/1.jpg%23center\" /></a><br />\r\n&quot;<br />\r\nThe Pragmatic Programmer is one of those rare tech books you&rsquo;ll read, re-read, and read again over the years. Whether you&rsquo;re new to the field or an experienced practitioner, you&rsquo;ll come away with fresh insights each and every time.</p>\r\n\r\n<p>Dave Thomas and Andy Hunt wrote the first edition of this influential book in 1999 to help their clients create better software and rediscover the joy of coding. These lessons have helped a generation of programmers examine the very essence of software development, independent of any particular language, framework, or methodology, and the Pragmatic philosophy has spawned hundreds of books, screencasts, and audio books, as well as thousands of careers and success stories.</p>\r\n\r\n<p>Now, twenty years later, this new edition re-examines what it means to be a modern programmer. Topics range from personal responsibility and career development to architectural techniques for keeping your code flexible and easy to adapt and reuse.&quot;<a href=\"https://amzn.to/3bO6Xyb\">Amazon.com</a></p>\r\n\r\n<h2>Conclusion</h2>\r\n\r\n<p>Although the order might suprise some, by definition, most of you must have heard of these books already.</p>\r\n\r\n<p>A few additional things I learned making this list:</p>\r\n\r\n<ul>\r\n	<li>Marting Fowler and Steve McConnell are the only author with several books in the list.</li>\r\n	<li>Cracking to Code interview is the most recent book on the list, released in 2015.</li>\r\n	<li><a href=\"https://amzn.to/321qbMx\">Python Programming</a>, by John Zelle was the most cited book focused on one language. It would have #5 had I taken it into account.</li>\r\n</ul>\r\n\r\n<p>I hope you enjoyed this article.</p>\r\n\r\n<p>I must admit, this one took a while to write. If you liked this article and feel like Twitter would like it, please <a href=\"https://twitter.com/intent/tweet?text=https%3A%2F%2Fwww.daolf.com%2Fposts%2Fbest-programming-books%2F%20by%20%40PierreDeWulf\">share it</a>, it really does help :).</p>\r\n\r\n<p><em>Note: while making this article, <a href=\"https://dev.to/awwsmm/20-most-recommended-books-for-software-developers-5578\">this one</a> appeared in the Google results. I ended up doing mine anyway because I would use a different automated aggregation technique that allowed be to compiled twice as much lists as he did. However, checking both list could be interesting :).</em></p>\r\n', 't3utqgh6lr4gf06t6pnf.webp', '2020-02-18 13:01:47');
INSERT INTO `articles` (`article_id`, `id_category`, `id_author`, `article_title`, `article_content`, `article_image`, `article_created_time`) VALUES
(43, 4, 1, 'Ask HN: Does Anybody Still Use JQuery?', '<h3>Lorem Ipsum: when, and when not to use it</h3>\r\n\r\n<p>Do you like Cheese Whiz? Spray tan? Fake eyelashes? That&#39;s what is Lorem Ipsum to many&mdash;it rubs them the wrong way, all the way. It&#39;s unreal, uncanny, makes you wonder if something is wrong, it seems to seek your attention for all the wrong reasons. Usually, we prefer the real thing, wine without sulfur based preservatives, real butter, not margarine, and so we&#39;d like our layouts and designs to be filled with real words, with thoughts that count, information that has value.</p>\r\n\r\n<p>The toppings you may chose for that TV dinner pizza slice when you forgot to shop for foods, the paint you may slap on your face to impress the new boss is your business. But what about your daily bread? Design comps, layouts, wireframes&mdash;will your clients accept that you go about things the facile way? Authorities in our business will tell in no uncertain terms that Lorem Ipsum is that huge, huge no no to forswear forever. Not so fast, I&#39;d say, there are some redeeming factors in favor of greeking text, as its use is merely the symptom of a worse problem to take into consideration.</p>\r\n\r\n<p><img alt=\"\" src=\"http://localhost/blog/webmag/img/post-4.jpg\" /></p>\r\n\r\n<p>So Lorem Ipsum is bad (not necessarily)</p>\r\n\r\n<p>You begin with a text, you sculpt information, you chisel away what&#39;s not needed, you come to the point, make things clear, add value, you&#39;re a content person, you like words. Design is no afterthought, far from it, but it comes in a deserved second. Anyway, you still use Lorem Ipsum and rightly so, as it will always have a place in the web workers toolbox, as things happen, not always the way you like it, not always in the preferred order. Even if your less into design and more into content strategy you may find some redeeming value with, wait for it, dummy copy, no less.</p>\r\n\r\n<p>There&#39;s lot of hate out there for a text that amounts to little more than garbled words in an old language. The villagers are out there with a vengeance to get that Frankenstein, wielding torches and pitchforks, wanting to tar and feather it at the least, running it out of town in shame.</p>\r\n\r\n<p>One of the villagers, Kristina Halvorson from Adaptive Path, holds steadfastly to the notion that design can&rsquo;t be tested without real content:</p>\r\n\r\n<blockquote>I&rsquo;ve heard the argument that &ldquo;lorem ipsum&rdquo; is effective in wireframing or design because it helps people focus on the actual layout, or color scheme, or whatever. What kills me here is that we&rsquo;re talking about creating a user experience that will (whether we like it or not) be DRIVEN by words. The entire structure of the page or app flow is FOR THE WORDS.</blockquote>\r\n\r\n<p>If that&#39;s what you think how bout the other way around? How can you evaluate content without design? No typography, no colors, no layout, no styles, all those things that convey the important signals that go beyond the mere textual, hierarchies of information, weight, emphasis, oblique stresses, priorities, all those subtle cues that also have visual and emotional appeal to the reader. Rigid proponents of content strategy may shun the use of dummy copy but then designers might want to ask them to provide style sheets with the copy decks they supply that are in tune with the design direction they require.</p>\r\n\r\n<h3>Summing up, if the copy is diverting attention from the design it&rsquo;s because it&rsquo;s not up to task.</h3>\r\n\r\n<p>Typographers of yore didn&#39;t come up with the concept of dummy copy because people thought that content is inconsequential window dressing, only there to be used by designers who can&rsquo;t be bothered to read. Lorem Ipsum is needed because words matter, a lot. Just fill up a page with draft copy about the client&rsquo;s business and they will actually read it and comment on it. They will be drawn to it, fiercely. Do it the wrong way and draft copy can derail your design review.</p>\r\n', 'post-page.jpg', '2020-08-05 16:11:14'),
(44, 15, 1, '10 Amazing JavaScript Games In Under 13kB of Code', '<h1>&nbsp;</h1>\r\n\r\n<p>In this fun article we&#39;ll take a look at the amazing&nbsp;<a href=\"https://js13kgames.com/\" target=\"_blank\">JS13K</a>&nbsp;game challenge. It is an annual coding competition where super talented JavaScript developers show off their games made with only 13kB of code or less.</p>\r\n\r\n<p>We&#39;ve chosen some of our favorite games from last year to share with you. Enjoy them as a tiny gaming break from work or as a source of coding inspiration!</p>\r\n\r\n<hr />\r\n<p><a href=\"https://js13kgames.com/games/onoff/index.html\" target=\"_blank\"><img alt=\"on-off.png\" src=\"https://tutorialzine.com/media/2018/11/on-off.png\" /></a></p>\r\n\r\n<h2><a href=\"https://js13kgames.com/games/onoff/index.html\" target=\"_blank\">OnOff</a></h2>\r\n\r\n<p>This is a challenging platformer with great design and fun music. Dodge spikes, jump over pits, and toggle between dimensions to complete all the 25 levels. The game also offers a cool level editor where you can create your own levels.</p>\r\n\r\n<hr />\r\n<p><a href=\"https://js13kgames.com/games/everyones-sky/index.html\" target=\"_blank\"><img alt=\"everyonesky.jpg\" src=\"https://tutorialzine.com/media/2019/02/everyonesky.jpg\" /></a></p>\r\n\r\n<h2><a href=\"https://js13kgames.com/games/everyones-sky/index.html\" target=\"_blank\">Everyone&#39;s sky</a></h2>\r\n\r\n<p>Everyone&#39;s sky offers the classic Asteroid style game but with an adventure and exploration RPG twist. You fly around space and its solar systems, contacting other civilizations and collecting resources. You can choose to complete missions peacefully, making allies, or just attack everything that comes in your way.</p>\r\n\r\n<hr />\r\n<p><a href=\"https://js13kgames.com/games/underrun/index.html\" target=\"_blank\"><img alt=\"underrun.png\" src=\"https://tutorialzine.com/media/2018/11/underrun.png\" /></a></p>\r\n\r\n<h2><a href=\"https://js13kgames.com/games/underrun/index.html\" target=\"_blank\">Underrun</a></h2>\r\n\r\n<p>A shooter game where you run in a destroyed lab and your goal is to kill the spider-looking enemies and find the terminals and reboot the systems. The game runs really smoothly, with nice pixelated graphics and great sound and lighting effects.</p>\r\n\r\n<hr />\r\n<p><a href=\"https://js13kgames.com/games/konnekt/index.html\" target=\"_blank\"><img alt=\"konnekt.png\" src=\"https://tutorialzine.com/media/2018/11/konnekt.png\" /></a></p>\r\n\r\n<h2><a href=\"https://js13kgames.com/games/konnekt/index.html\" target=\"_blank\">Konnekt</a></h2>\r\n\r\n<p>This is a simple real-time-strategy game where a virus has infected some of your system&#39;s nodes and you need to clear them before they spread. The game is really addicting and the more you play, the harder it gets and the more nodes you have to deal with.</p>\r\n\r\n<hr />\r\n<p><a href=\"https://js13kgames.com/games/super-chrono-portal-maker/index.html\" target=\"_blank\"><img alt=\"super-chrono-portal-maker.png\" src=\"https://tutorialzine.com/media/2018/11/super-chrono-portal-maker.png\" /></a></p>\r\n\r\n<h2><a href=\"https://js13kgames.com/games/super-chrono-portal-maker/index.html\" target=\"_blank\">Super Chrono Portal Maker</a></h2>\r\n\r\n<p>An hommage to the original Super Mario games, this platformer offers 30 levels of run and jump action. Each level introduces more and more game mechanics, making the game super fun to play. There is also a level builder where you can create your own levels and share them with friends.</p>\r\n\r\n<hr />\r\n<p><a href=\"https://js13kgames.com/games/offline-paradise/index.html\" target=\"_blank\"><img alt=\"offline-paradise.png\" src=\"https://tutorialzine.com/media/2018/11/offline-paradise.png\" /></a></p>\r\n\r\n<h2><a href=\"https://js13kgames.com/games/offline-paradise/index.html\" target=\"_blank\">Offline Paradise</a></h2>\r\n\r\n<p>A fast-paced paltformer where you need to run, jump or dash over different obstacles. An awesome feature of this game are the automatic checkpoint which bring you right back into the action if you fail on one of the challenges. It runs very smoothly with a constant high FPS while still having a pleasing parallax background, animations and music.</p>\r\n\r\n<hr />\r\n<p><a href=\"https://js13kgames.com/games/raven/index.html\" target=\"_blank\"><img alt=\"raven.png\" src=\"https://tutorialzine.com/media/2018/11/raven.png\" /></a></p>\r\n\r\n<h2><a href=\"https://js13kgames.com/games/raven/index.html\" target=\"_blank\">Raven</a></h2>\r\n\r\n<p>Raven is a action-puzzle game where you need to fix the security cameras of the secret facility. There are mysterious creatures that you need to keep an eye on, or else they will kill you in the dark.</p>\r\n\r\n<hr />\r\n<p><a href=\"https://js13kgames.com/games/systems-offline/index.html\" target=\"_blank\"><img alt=\"systems-offline.png\" src=\"https://tutorialzine.com/media/2018/11/systems-offline.png\" /></a></p>\r\n\r\n<h2><a href=\"https://js13kgames.com/games/systems-offline/index.html\" target=\"_blank\">Systems Offline</a></h2>\r\n\r\n<p>Awesome puzzle game in which you are stuck on a broken space station. Your mission is to fix the systems and escape before your oxygen runs out. The game can be played with a mouse or touch controls, offers colorblind mode and has three different difficulty levels.</p>\r\n\r\n<hr />\r\n<p><a href=\"https://js13kgames.com/games/re-wire/index.html\" target=\"_blank\"><img alt=\"rewire.png\" src=\"https://tutorialzine.com/media/2018/11/rewire.png\" /></a></p>\r\n\r\n<h2><a href=\"https://js13kgames.com/games/re-wire/index.html\" target=\"_blank\">Re-wire</a></h2>\r\n\r\n<p>This game tests your logical thinking skills. You need to connect the nodes with the cable and plug it into the socket, without touching the blades. It is really challenging and fun and the best part is that it saves automatically and you can play it multiple times without losing your progress.</p>\r\n\r\n<hr />\r\n<p><a href=\"https://js13kgames.com/games/off-the-line/index.html\" target=\"_blank\"><img alt=\"off-the-line.png\" src=\"https://tutorialzine.com/media/2018/11/off-the-line.png\" /></a></p>\r\n\r\n<h2><a href=\"https://js13kgames.com/games/off-the-line/index.html\" target=\"_blank\">Off The Line</a></h2>\r\n\r\n<p>Off the line is one of those games that have a very simple concept, while being super fun to play and difficult to master. There are 20 stages and 3 difficulty levels that you can try if you are looking for a challenge.</p>\r\n\r\n<hr />\r\n<p><a href=\"https://js13kgames.com/games/1024-moves/index.html\" target=\"_blank\"><img alt=\"1024-moves.png\" src=\"https://tutorialzine.com/media/2018/11/1024-moves.png\" /></a></p>\r\n\r\n<h2><a href=\"https://js13kgames.com/games/1024-moves/index.html\" target=\"_blank\">1024 Moves</a></h2>\r\n\r\n<p>This is an amazing 3D puzzle game where you have the task of moving a cube through a tile grid and reach the endpoint. Avoid the holes, move different objects and try to complete all the levels in less than 1024 moves.</p>\r\n\r\n<hr />\r\n<p><a href=\"https://js13kgames.com/games/spacecraft/index.html\" target=\"_blank\"><img alt=\"spacecraft.png\" src=\"https://tutorialzine.com/media/2018/11/spacecraft.png\" /></a></p>\r\n\r\n<h2><a href=\"https://js13kgames.com/games/spacecraft/index.html\" target=\"_blank\">Spacecraft</a></h2>\r\n\r\n<p>Spacecraft is an interesting game where you need to collect as many tokens as you can from the planets of the Solar System. You need to stay on track, dodge obstacles and asteroids, and spend your tokens wisely to upgrade your ship.</p>\r\n\r\n<hr />\r\n<p><strong>Bootstrap Studio</strong></p>\r\n\r\n<p>The revolutionary web design tool for creating responsive websites and apps.</p>\r\n\r\n<p><a href=\"https://tutorialzine.com/2019/02/10-amazing-javascript-games#\">LEARN MORE</a></p>\r\n', '10-amazing-js-games.png', '2020-08-06 14:58:20'),
(45, 16, 1, '10 Useful Git Tips', '<p>Over the past few years git has vastly grown in popularity to become one of the most used version control systems. It is used by developers coding in various languages and teams of all sizes, from small open-source projects to huge codebases like the&nbsp;<a href=\"https://github.com/torvalds/linux\" target=\"_blank\">linux kernel</a>.</p>\r\n\r\n<p>In this article we are going to share with you a few tips that could improve your git experience and workflow.</p>\r\n\r\n<hr />\r\n<h2><a href=\"https://git-scm.com/docs/git-log#git-log---no-merges\" target=\"_blank\">git log --no-merges</a></h2>\r\n\r\n<p>This git command shows the whole commit history but skips commits that merged two branches together or solve a merge conflict. This allows you to quickly see all the changes done to the project, without having merge commits cluttering the git history.</p>\r\n\r\n<pre>\r\n$git log --no-merges\r\n\r\ncommit e75fe8bf2c5c46dbd9e1bc20d2f8b2ede81f2d93\r\nAuthor:  John\r\nDate:   Mon Jul 10 18:04:50 2017 +0300\r\n\r\n    Add new branch.\r\n\r\ncommit 080dfd342ab0dbdf69858e3b01e18584d4eade34\r\nAuthor:  John\r\nDate:   Mon Jul 11 15:40:56 2017 +0300\r\n\r\n    Added index.php.\r\n\r\ncommit 2965803c0deeac1f2427ec2f5394493ed4211655\r\nAuthor:  John\r\nDate:   Mon Jul 13 12:14:50 2017 +0300\r\n\r\n    Added css files.\r\n\r\n</pre>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<hr />\r\n<h2><a href=\"https://git-scm.com/docs/git-revert#git-revert--n\" target=\"_blank\">git revert --no-commit [commit]</a></h2>\r\n\r\n<p>Git revert generates a new commit that undoes the changes made by existing commits and generates a new commit with the resulting content. If you want to to revert the named commits and avoid the automatic commits, you can use the flag --no-commit or the shorthand -n.</p>\r\n\r\n<hr />\r\n<h2><a href=\"https://git-scm.com/docs/git-diff#git-diff--w\" target=\"_blank\">git diff -w</a></h2>\r\n\r\n<p><code>Git diff</code>&nbsp;shows the changes between two commits, two working trees or two files on disk. When multiple people work on the same project, often there are changes due to text editor&#39;s tab and space setting. In order to ignore differences caused by whitespaces when comparing lines, you can use it with the -w flag.</p>\r\n\r\n<hr />\r\n<h2><a href=\"https://git-scm.com/docs/git-diff#git-diff---statltwidthgtltname-widthgtltcountgt\" target=\"_blank\">git diff --stat</a></h2>\r\n\r\n<p>Shows how each file has been changed over time. You can add 3 parameters:&nbsp;<strong>width</strong>&nbsp;to override the default output width,&nbsp;<strong>name-width</strong>&nbsp;to set the width of the filename and&nbsp;<strong>count</strong>&nbsp;to limit the output to the first number of lines.</p>\r\n\r\n<pre>\r\n$ git diff --stat\r\n index.php | 83 +++++++++++++++++++++++++++++---------------------------\r\n 1 file changed, 43 insertions(+), 40 deletions(-)\r\n\r\n</pre>\r\n\r\n<pre>\r\n$ git diff --stat-width=10\r\n index.php | 83 +++---\r\n 1 file changed, 43 insertions(+), 40 deletions(-)\r\n</pre>\r\n\r\n<hr />\r\n<h2><a href=\"https://git-scm.com/docs/git-reset#git-reset---soft\" target=\"_blank\">git reset --soft HEAD^</a></h2>\r\n\r\n<p>Reset the head to a certain commit without touching the index file and the working tree. All changes made after this commit are moved to &quot;staged for commit&quot; stage. After that you just need to run&nbsp;<code>git commit</code>&nbsp;to add them back in.</p>\r\n\r\n<hr />\r\n<h2><a href=\"https://git-scm.com/docs/git-stash#git-stash-branchltbranchnamegtltstashgt\" target=\"_blank\">git stash branch [branch-name] [stash]</a></h2>\r\n\r\n<p>This command creates a new branch named branch-name and check it out, then applies the changes from the given stash to it and drops the stash. If no stash is given, it uses the latest one. This allows you to apply any stashed changes into a safer environment, that can later be merged into master.</p>\r\n\r\n<hr />\r\n<h2><a href=\"https://git-scm.com/docs/git-branch#git-branch--a\" target=\"_blank\">git branch -a</a></h2>\r\n\r\n<p>It shows a list of all remote-tracking and local branches. You can use the --merged flag to see only the branches that are fully merged to the master branch. This way you can track your branches and find out which ones aren&#39;t used anymore and can be deleted.</p>\r\n\r\n<pre>\r\n$ git branch -a\r\n\r\n  dev\r\n* master\r\n  remotes/origin/HEAD -&gt; origin/master\r\n  remotes/origin/dev\r\n</pre>\r\n\r\n<hr />\r\n<h2><a href=\"https://git-scm.com/docs/git-commit#git-commit---amend\" target=\"_blank\">git commit --amend</a></h2>\r\n\r\n<p>With&nbsp;<code>git commit --amend</code>&nbsp;you can change your previous commit, instead of making a new one. If you haven&#39;t pushed your changes to a remote branch, you can use this command to amend the most recent commit, adding your latest changes and even changing your commit message.</p>\r\n\r\n<hr />\r\n<h2><a href=\"https://git-scm.com/docs/git-pull#git-pull---rebasefalsetruepreserveinteractive\" target=\"_blank\">git pull --rebase</a></h2>\r\n\r\n<p>Git pull --rebase forces git to first pull the changes and then rebase the unpushed commits on top of the latest version of the remote branch. The --rebase option can be used to ensure a linear history by preventing unnecessary merge commits.</p>\r\n\r\n<hr />\r\n<h2><a href=\"https://git-scm.com/docs/git-add#git-add--p\" target=\"_blank\">git add -p</a></h2>\r\n\r\n<p>When you use this command, instead of immediately adding all the changed to the index, it goes through each change and asks what you want to do with it. This way, it allows you to interactively choose exactly what you want to be committed.</p>\r\n\r\n<pre>\r\ndiff --git a/package.json b/package.json\r\nindex db78332..a814f7e 100644\r\n--- a/package.json\r\n+++ b/package.json\r\n@@ -6,7 +6,6 @@\r\n   },\r\n   &quot;devDependencies&quot;: {\r\n     &quot;bootstrap-sass&quot;: &quot;^3.3.7&quot;,\r\n-    &quot;gulp&quot;: &quot;^3.9.1&quot;,\r\n     &quot;jquery&quot;: &quot;^3.1.0&quot;,\r\n     &quot;laravel-elixir&quot;: &quot;^6.0.0-11&quot;,\r\n     &quot;laravel-elixir-vue-2&quot;: &quot;^0.2.0&quot;,\r\nStage this hunk [y,n,q,a,d,/,e,?]? </pre>\r\n', '10-useful-git-tips.png', '2020-08-06 16:45:08'),
(46, 15, 2, 'Creating an Image Zoom Library With Vanilla JavaScript', '<p>In this tutorial we are going to build a simple JavaScript library for adding zoom-on-hover effects to images. We will make the whole library from scratch, without relying on jQuery or any other external dependencies. Let&#39;s jump right in!</p>\r\n\r\n<h2>The Project</h2>\r\n\r\n<p>You can see this type of effect on many shopping sites, including very popular ones like <a href=\"https://www.ebay.com/\" target=\"_blank\">eBay</a> and <a href=\"https://www.amazon.com/\" target=\"_blank\">Amazon</a>. It usually consists of a group of small photos which can be enlarged and inspected in further detail with an on-hover magnifier.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<div>\r\n<figure><video loop=\"\" autoplay=\"\" controls=\"\" src=\"https://tutorialzine.com/media/2017/08/vanilla-zoom-demo.mp4\"></video><figcaption>Our Library in Action</figcaption></figure>\r\n</div>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>To keep the tutorial simple we won&#39;t be adding too many features to the library. It will contain only one JavaScript file, plus an optional CSS file for quickly styling a gallery like the one above.</p>\r\n\r\n<h2>Designing the Library</h2>\r\n\r\n<p>Before we start building the library, let&#39;s take a look at how we want people to use it. Making that design decision first will make developing the actual library easier later on.</p>\r\n\r\n<p>Since we are making a gallery plugin, people who use it will need to have some boilerplate HTML. This markup will contain their images, an empty <code>div</code> for the zoom effect, as well as some predefined classes to make the library work.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n\r\n<p>People are free to change this layout and add as many images as they want. It&#39;s important, however, that every image has the <code>.small-preview</code> class, and that there is an empty div with the <code>.zoomed-image</code> class.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>The library will be mainly JavaScript driven but there are also a few important CSS styles that need to be set. Users can include our CSS file directly in their HTML.</p>\r\n\r\n<pre>\r\n\r\n&nbsp;</pre>\r\n\r\n<p>Now that the markup and styles are set, all that is left is to include the library and initialize it.</p>\r\n\r\n<pre>\r\n\r\n&nbsp;</pre>\r\n\r\n<p>Including the library&#39;s .js file makes the <code>vanillaZoom</code> object globally available. The object has only one method which is for initializing the plugin. It takes a single parameter - the id of our gallery. This way we can have multiple independent galleries initialized on one page.</p>\r\n\r\n<h2>Developing the Library</h2>\r\n\r\n<p>When building front-end JavaScript libraries we need to make sure we register their API properly. There are many ways to do this, possibly the easiest of which is <a href=\"http://checkman.io/blog/creating-a-javascript-library/\" target=\"_blank\">this method</a> by Jordan Checkman. We advise you to read his full blog post, but in short it boils down to this:</p>\r\n\r\n<pre>\r\n<code>\r\n(function(window) {\r\n  function define_library() {\r\n    // Create the library object and all its properties and methods.\r\n    var vanillaZoom = {};\r\n    vanillaZoom.init = function(galleryId) {\r\n      // Our library&#39;s logic goes here.\r\n    }\r\n    return vanillaZoom;\r\n  }\r\n\r\n  // Add the vanillaZoom object to global scope if its not already defined.\r\n  if(typeof(vanillaZoom) === &#39;undefined&#39;) {\r\n    window.vanillaZoom = define_library();\r\n  }\r\n  else{\r\n    console.log(&quot;Library already defined.&quot;);\r\n  }\r\n})(window);</code></pre>\r\n\r\n<p>The above code is wrapped in a self executing function. This way when we add the <code>vanilla-zoom.js</code> file to our project, the library will be automatically registered and the <code>vanillaZoom</code> object with all its methods will be made available to the user.</p>\r\n\r\n<p>Our library has only one method - <code>vanillaZoom.init(galleryId)</code>. Its job is to select the gallery DOM elements and add event listeners to them.</p>\r\n\r\n<p>First we check if the proper elements have been added to the HTML and select them. We can&#39;t use jQuery so we have to rely on the native JavaScript methods for working with the DOM.</p>\r\n\r\n<pre>\r\nvar container = document.querySelector(el);\r\n\r\nif(!container) {\r\n    console.error(&#39;Please specify the correct class of your gallery.&#39;);\r\n    return;\r\n}\r\n\r\nvar firstSmallImage = container.querySelector(&#39;.small-preview&#39;);\r\nvar zoomedImage = container.querySelector(&#39;.zoomed-image&#39;);\r\n\r\nif(!zoomedImage) {\r\n    console.error(&#39;Please add a .zoomed-image element to your gallery.&#39;);\r\n    return;\r\n}\r\n\r\nif(!firstSmallImage) {\r\n    console.error(&#39;Please add images with the .small-preview class to your gallery.&#39;);\r\n    return;\r\n}\r\nelse {\r\n    // Set the source of the zoomed image.\r\n    zoomedImage.style.backgroundImage = &#39;url(&#39;+ firstSmallImage.src +&#39;)&#39;;\r\n}</pre>\r\n\r\n<p>In the last line of the above code we take the image source of one of the preview images and set it as the background of our zoomable element. This happens as soon as <code>vanillaZoom.init(galleryId)</code> is called, making sure that our gallery doesn&#39;t stay empty.</p>\r\n\r\n<p>We do the same when one of the previews is clicked. This allows the user to select which image they want magnified.</p>\r\n\r\n<pre>\r\ncontainer.addEventListener(&quot;click&quot;, function (event) {\r\n  var elem = event.target;\r\n\r\n  if (elem.classList.contains(&quot;small-preview&quot;)) {\r\n      zoomedImage.style.backgroundImage = &#39;url(&#39;+ elem.src +&#39;)&#39;;\r\n  }\r\n});\r\n</pre>\r\n\r\n<p>Selecting Images</p>\r\n\r\n<p>The magnifier element has a couple of event listeners attached to it. The first one is activated when the cursor enters the element, increasing the size of the background image, thus creating a zoom effect.</p>\r\n\r\n<pre>\r\nzoomedImage.addEventListener(&#39;mouseenter&#39;, function(e) {\r\n    this.style.backgroundSize = &quot;250%&quot;; \r\n}, false);</pre>\r\n\r\n<p>Since the image is now very large, it won&#39;t fit in the container and only part of it will be visible. We want users to be able to select which portion of the image is magnified so we add a mousemove listener that changes the background position.</p>\r\n\r\n<pre>\r\nzoomedImage.addEventListener(&#39;mousemove&#39;, function(e) {\r\n\r\n  // getBoundingClientReact gives us various information about the position of the element.\r\n  var dimentions = this.getBoundingClientRect();\r\n\r\n  // Calculate the position of the cursor inside the element (in pixels).\r\n  var x = e.clientX - dimentions.left;\r\n  var y = e.clientY - dimentions.top;\r\n\r\n  // Calculate the position of the cursor as a percentage of the total size of the element.\r\n  var xpercent = Math.round(100 / (dimentions.width / x));\r\n  var ypercent = Math.round(100 / (dimentions.height / y));\r\n\r\n  // Update the background position of the image.\r\n  this.style.backgroundPosition = xpercent+&#39;% &#39; + ypercent+&#39;%&#39;;\r\n\r\n}, false);</pre>\r\n\r\n<p>Magnifying Specific Part of Image</p>\r\n\r\n<p>When the cursor leaves the magnified image we want it to go back to normal. This is easily done by returning the background size to <code>cover</code> and the background position to <code>center</code>.</p>\r\n\r\n<pre>\r\nzoomedImage.addEventListener(&#39;mouseleave&#39;, function(e) {\r\n    this.style.backgroundSize = &quot;cover&quot;; \r\n    this.style.backgroundPosition = &quot;center&quot;; \r\n}, false);</pre>\r\n\r\n<p>And with that, we are done!</p>\r\n\r\n<h2>Browser Support</h2>\r\n\r\n<p>The library should work in all modern desktop browsers, although some of the flexbox CSS may not be displayed properly on older IE.</p>\r\n\r\n<p>Sadly, the zoom effect doesn&#39;t translate very well to touch devises. Because of this and the limited screen space, it&#39;s best to present your images in another way for mobile. In our CSS we&#39;ve simply hidden the zoom element and listed the images vertically but you can try other solutions such as a carousel.</p>\r\n\r\n<h2>Conclusion</h2>\r\n\r\n<p>You can get the full source code for this article, as well as the demo code (with images courtesy to <a href=\"https://burst.shopify.com/\" target=\"_blank\">Burst</a>), from the <strong>Download</strong> button near the top of the page. You are free to use the library in all your projects, commercial or personal (<a href=\"https://tutorialzine.com/license\" target=\"_blank\">our license</a>). Happy coding!</p>\r\n', 'image-zoom-library-with-vanilla-js.jpg', '2020-08-07 19:10:22'),
(47, 1, 8, 'Get Instagram Feed Data without Code', '<p>Ever had to access Instagram API and had to go through the confusing facebook docs, I have seen a friend go through the Facebook docs for hours and still not able to get it working. We need an easier way to access Instagram API, it could be to embed it on our portfolio or basically to do anything with it.</p>\r\n\r\n<p><a href=\"https://res.cloudinary.com/practicaldev/image/fetch/s--S2iZncnq--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_66%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/esgjssiai5cvu3omzyte.gif\"><img alt=\"Gif\" src=\"https://res.cloudinary.com/practicaldev/image/fetch/s--S2iZncnq--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_66%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/esgjssiai5cvu3omzyte.gif\" /></a></p>\r\n\r\n<p>Me trying to understand Facebook docs.</p>\r\n\r\n<p>So yeah, this is exactly what we are trying to solve with <a href=\"https://instafeedapi.com\">InstaFeedAPI</a>, no need to go through the confusing docs or read every single thing and pass weird parameters with no explanation to get your own feed.</p>\r\n\r\n<p>So we have read the docs for you &amp; we have done it the right way, all you need is the token for your Instagram account and that&#39;s it (literally). You just paste it in our dashboard and get an API.</p>\r\n\r\n<h3>What are we doing today?</h3>\r\n\r\n<ol>\r\n	<li>Get your Instagram Long-lived token</li>\r\n	<li>Fill a two-step form</li>\r\n	<li>Voila, you have an Instagram feed API ready.</li>\r\n</ol>\r\n\r\n<h2>Get the token</h2>\r\n\r\n<h3>1. Make a Facebook app</h3>\r\n\r\n<p>Open Facebook Developer portal<br />\r\n<a href=\"https://developers.facebook.com/apps/\">https://developers.facebook.com/apps/</a></p>\r\n\r\n<p>If you&#39;re doing this for the first time, join the FB developers portal by logging in.</p>\r\n\r\n<p>Click on <strong>Get Started on top</strong> -&gt; <strong>Verify your account</strong></p>\r\n\r\n<p>After clicking on <strong>Create App</strong>, click on <strong>&quot;For Everything Else&quot;</strong> from the popup. Give your App a Name.</p>\r\n\r\n<p><a href=\"https://res.cloudinary.com/practicaldev/image/fetch/s--WDefbS5J--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/0ydb65laghkukpg844a1.png\"><img alt=\"Instagram APP\" src=\"https://res.cloudinary.com/practicaldev/image/fetch/s--WDefbS5J--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/0ydb65laghkukpg844a1.png\" /></a></p>\r\n\r\n<h3>2. Add Instagram Testers</h3>\r\n\r\n<p>Now select <strong>Instagram</strong> from the set of Facebook products, as we want the Instagram feed.</p>\r\n\r\n<p>You will be taken to an agreement page, select <strong>Basic Display</strong> from the sidebar click on <strong>Create App</strong>, give it a name again and you will now be seeing the Instagram developer console.</p>\r\n\r\n<p>Scroll down and you will be able to see a section called <strong>User Token Generator</strong> &amp; Click on <strong>Add or Remove Instagram Testers.</strong></p>\r\n\r\n<p><a href=\"https://res.cloudinary.com/practicaldev/image/fetch/s--ibqvk49z--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/vpu3fayg3a07y5ztapoe.png\"><img alt=\"Add Tester\" src=\"https://res.cloudinary.com/practicaldev/image/fetch/s--ibqvk49z--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/vpu3fayg3a07y5ztapoe.png\" /></a></p>\r\n\r\n<p>Scroll down and add the Instagram username of whose feed you want in the API, <strong>this only works for accounts that are set as public and you will not be able to access private accounts.</strong></p>\r\n\r\n<p>Let&#39;s add our own account username and accept the testing invite by accessing this link <a href=\"https://www.instagram.com/accounts/manage_access/\"><code>https://www.instagram.com/accounts/manage_access/</code></a></p>\r\n\r\n<p>Once done come back to the Facebook developer portal, open your app, click on <strong>Basic Display</strong> and click <strong>Generate Token</strong>.</p>\r\n\r\n<p>You now have finally generated an <strong>Instagram access token</strong>, keep this somewhere aside.</p>\r\n\r\n<h3>Video Tutorial about getting Token</h3>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>Insta Feed API.</h2>\r\n\r\n<p>So, all you have to do is create an account in <a href=\"https://instafeedapi.com/signup\">https://instafeedapi.com/signup</a> and give yourself a username.</p>\r\n\r\n<p><a href=\"https://res.cloudinary.com/practicaldev/image/fetch/s--Sl4pJFbl--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/rkgz7jyj7d6swhgai3aw.jpeg\"><img alt=\"Setup Account\" src=\"https://res.cloudinary.com/practicaldev/image/fetch/s--Sl4pJFbl--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/rkgz7jyj7d6swhgai3aw.jpeg\" /></a></p>\r\n\r\n<p>Click on Create Instagram API button, once done you will be seeing a form like below, just give your API a name and enter your Instagram token.</p>\r\n\r\n<p><a href=\"https://res.cloudinary.com/practicaldev/image/fetch/s--261oErXI--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/1vopoeccpdb4jhk500nk.jpeg\"><img alt=\"Make API\" src=\"https://res.cloudinary.com/practicaldev/image/fetch/s--261oErXI--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/1vopoeccpdb4jhk500nk.jpeg\" /></a></p>\r\n\r\n<p>Your API will be listed and you can create more APIs with different accounts if you like.</p>\r\n\r\n<p>Click on the API and there you have it an API endpoint which will return you a list of your Instagram feed in a clean JSON format.</p>\r\n\r\n<p><a href=\"https://res.cloudinary.com/practicaldev/image/fetch/s--GxAPDGcR--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/4tgfjk28ts7tbxpqwvdh.jpeg\"><img alt=\"InstaFeedAPI docs\" src=\"https://res.cloudinary.com/practicaldev/image/fetch/s--GxAPDGcR--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/4tgfjk28ts7tbxpqwvdh.jpeg\" /></a></p>\r\n\r\n<p>You can also pass limit, before &amp; after parameters to our endpoint to have quick filters.</p>\r\n\r\n<p>Website: <a href=\"https://instafeedapi.com\">https://instafeedapi.com</a><br />\r\nPricing Page: <a href=\"https://instafeedapi.com/pricing\">https://instafeedapi.com/pricing</a></p>\r\n', 'dwvsjs9gyl4szykr0ppl.png', '2020-08-07 20:05:41');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banners`
--

CREATE TABLE `banners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `creator_id` bigint(20) UNSIGNED DEFAULT NULL,
  `image_id` int(10) UNSIGNED NOT NULL,
  `order` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `title` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `target` varchar(255) NOT NULL DEFAULT '_blank',
  `description` text DEFAULT NULL,
  `enabled_at` timestamp NULL DEFAULT NULL,
  `expired_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncar tablas antes de insertar `banners`
--

TRUNCATE TABLE `banners`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

CREATE TABLE `categories` (
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `category_name` varchar(100) NOT NULL,
  `category_image` varchar(255) NOT NULL,
  `category_color` varchar(10) NOT NULL DEFAULT '333333'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Truncar tablas antes de insertar `categories`
--

TRUNCATE TABLE `categories`;
--
-- Volcado de datos para la tabla `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`, `category_image`, `category_color`) VALUES
(1, 'Web Development', '1 9npNPVH7iNJ64Koq7EcW5A.jpeg', '#4BB92F'),
(2, 'CSS', 'android.png', '#0078ff'),
(4, 'JQuery', 'dataScience.jpg', '#8d00ff'),
(14, 'Front End Dev', 'Front-end-developemtn-1.png', '#247ba0'),
(15, 'Javascript', 'sasasasa', '#ff8700'),
(16, 'Git', '', '#5bd770');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comments`
--

CREATE TABLE `comments` (
  `comment_id` bigint(20) UNSIGNED NOT NULL,
  `id_article` bigint(20) UNSIGNED NOT NULL,
  `comment_username` varchar(100) NOT NULL,
  `comment_avatar` varchar(255) NOT NULL DEFAULT 'def_face.jpg',
  `comment_content` text NOT NULL,
  `comment_date` datetime NOT NULL DEFAULT '2020-02-14 10:28:00',
  `comment_likes` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Truncar tablas antes de insertar `comments`
--

TRUNCATE TABLE `comments`;
--
-- Volcado de datos para la tabla `comments`
--

INSERT INTO `comments` (`comment_id`, `id_article`, `comment_username`, `comment_avatar`, `comment_content`, `comment_date`, `comment_likes`) VALUES
(1, 40, '250871902', 'def_face.jpg', 'This is a comment test 1', '2019-07-07 09:28:00', 0),
(2, 27, '989786379', 'def_face.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic', '2019-03-15 10:28:00', 0),
(3, 26, '378052515', 'def_face.jpg', 'This is an advanceed test', '2020-02-14 10:28:00', 0),
(4, 26, '378052515', 'def_face.jpg', 'This is an advanced test 2', '2020-01-05 11:28:00', 0),
(5, 40, '784031346', 'def_face.jpg', 'this is a comment 2', '2019-05-21 11:28:00', 0),
(6, 40, '2076925118', 'def_face.jpg', 'This is a comment test 5\r\nThis is a comment test 4\r\nThis is a comment test 3', '2020-02-18 17:16:12', 0),
(7, 6, '1249945059', 'def_face.jpg', 'Thank you for taking the time to write such an elaborate advice!🙌', '2020-02-18 17:16:50', 0),
(8, 17, '1884119267', 'def_face.jpg', 'this is s comment 1', '2020-02-18 17:21:43', 0),
(9, 41, '418132487', 'def_face.jpg', 'sasajsasas', '2020-02-18 17:46:10', 0),
(10, 27, '577317656', 'def_face.jpg', 'kdjkzhdzdjizjdz', '2020-02-19 11:34:23', 0),
(11, 26, '468461801', 'def_face.jpg', 'sasasasasasasasa', '2020-08-05 14:48:37', 0),
(12, 40, '1931903981', 'def_face.jpg', 'Test Comment', '2020-08-07 15:00:04', 0),
(13, 40, '1918947887', 'def_face.jpg', 'sasasasajsasa', '2020-08-07 19:37:34', 0),
(14, 40, '202906189', 'def_face.jpg', 'test comment', '2020-08-07 19:38:22', 0),
(15, 40, '1835186045', 'def_face.jpg', 'sajshasasa', '2020-08-07 19:38:44', 0),
(16, 40, '910296642', 'def_face.jpg', 'sasasas', '2020-08-07 19:38:56', 0),
(17, 40, '564956375', 'def_face.jpg', 'hhhhhhhhhhhhhhhh', '2020-08-07 19:39:13', 0),
(18, 40, '697303869', 'def_face.jpg', 'Test comment', '2021-10-07 17:53:32', 0),
(19, 44, '500715459', 'def_face.jpg', 'Test comment', '2020-08-07 19:56:21', 0),
(20, 44, '1893422093', 'def_face.jpg', 'yyyyyyyyyyyyyyyyyyyy', '2020-08-07 20:00:02', 0),
(21, 47, '1397021679', 'def_face.jpg', 'hcuzifzefd', '2020-08-07 20:32:34', 0),
(22, 47, '1613781666', 'def_face.jpg', 'salam', '2020-08-07 20:43:06', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `distritos_municipales`
--

CREATE TABLE `distritos_municipales` (
  `distrito_id` int(11) NOT NULL,
  `municipio_id` int(11) DEFAULT NULL,
  `nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Truncar tablas antes de insertar `distritos_municipales`
--

TRUNCATE TABLE `distritos_municipales`;
--
-- Volcado de datos para la tabla `distritos_municipales`
--

INSERT INTO `distritos_municipales` (`distrito_id`, `municipio_id`, `nombre`) VALUES
(1, 2, 'Barreras'),
(2, 2, 'Barro Arriba'),
(3, 2, 'Clavellina'),
(4, 2, 'Emma Balaguer Viuda Vallejo'),
(5, 2, 'Las Barías-La Estancia'),
(6, 2, 'Las Lomas'),
(7, 2, 'Los Jovillos'),
(8, 2, 'Puerto Viejo'),
(9, 5, 'Hatillo'),
(10, 5, 'Palmar de Ocoa'),
(11, 6, 'Villarpando'),
(12, 6, 'Hato Nuevo-Cortés'),
(13, 7, 'La Siembra'),
(14, 7, 'Las Lagunas'),
(15, 7, 'Los Fríos'),
(16, 9, 'El Rosario'),
(17, 10, 'Proyecto 4'),
(18, 10, 'Ganadero'),
(19, 10, 'Proyecto 2-C'),
(20, 11, 'Amiama Gómez'),
(21, 11, 'Los Toros'),
(22, 11, 'Tábara Abajo'),
(23, 12, 'El Palmar'),
(24, 13, 'El Salado'),
(25, 14, 'Las Clavellinas'),
(26, 15, 'Cabeza de Toro'),
(27, 15, 'Mena'),
(28, 15, 'Monserrat'),
(29, 15, 'Santa Bárbara-El 6'),
(30, 15, 'Santana'),
(31, 15, 'Uvilla'),
(32, 17, 'El Cachón'),
(33, 17, 'La Guázara'),
(34, 17, 'Villa Central'),
(35, 20, 'Arroyo Dulce'),
(36, 21, 'Pescadería'),
(37, 22, 'Palo Alto'),
(38, 23, 'Bahoruco'),
(39, 25, 'Los Patos'),
(40, 27, 'Canoa'),
(41, 27, 'Fondo Negro'),
(42, 27, 'Quita Coraza'),
(43, 28, 'Cañongo'),
(44, 4, 'Manuel Bueno'),
(45, 30, 'Capotillo'),
(46, 30, 'Santiago de la Cruz'),
(47, 33, 'Cenoví'),
(48, 33, 'Jaya'),
(49, 33, 'La Peña'),
(50, 33, 'Presidente Don Antonio Guzmán Fernández'),
(51, 34, 'Aguacate'),
(52, 34, 'Las Coles'),
(53, 36, 'Sabana Grande'),
(54, 39, 'Agua Santa del Yuna'),
(55, 39, 'Barraquito'),
(56, 39, 'Cristo Rey de Guaraguao'),
(57, 39, 'Las Táranas'),
(58, 40, 'Pedro Sánchez'),
(59, 40, 'San Francisco-Vicentillo'),
(60, 41, 'Santa Lucía'),
(61, 41, 'El Cedro'),
(62, 41, 'La Gina'),
(63, 42, 'Guayabo'),
(64, 42, 'Sabana Larga'),
(65, 43, 'Sabana Cruz'),
(66, 43, 'Sabana Higüero'),
(67, 44, 'Guanito'),
(68, 45, 'Rancho de la Guardia'),
(69, 47, 'Río Limpio'),
(70, 48, 'Canca La Reina'),
(71, 48, 'El Higüerito'),
(72, 48, 'José Contreras'),
(73, 48, 'Juan López'),
(74, 48, 'La Ortega'),
(75, 48, 'Las Lagunas'),
(76, 48, 'Monte de la Jagua'),
(77, 48, 'San Víctor'),
(78, 50, 'Joba Arriba'),
(79, 50, 'Veragua'),
(80, 50, 'Villa Magante'),
(81, 52, 'Guayabo Dulce'),
(82, 52, 'Mata Palacio'),
(83, 52, 'Yerba Buena'),
(84, 54, 'Elupina Cordero de Las Cañitas'),
(85, 55, 'Jamao Afuera'),
(86, 56, 'Blanco'),
(87, 58, 'Boca de Cachón'),
(88, 58, 'El Limón'),
(89, 59, 'Batey 8'),
(90, 60, 'Vengan a Ver'),
(91, 62, 'La Colonia'),
(92, 63, 'Guayabal'),
(93, 64, 'La Otra Banda'),
(94, 64, 'Lagunas de Nisibón'),
(95, 64, 'Verón-Punta Cana'),
(96, 65, 'Bayahibe'),
(97, 65, 'Boca de Yuma'),
(98, 66, 'Caleta'),
(99, 68, 'Cumayasa'),
(100, 69, 'El Ranchito'),
(101, 69, 'Río Verde Arriba'),
(102, 70, 'La Sabina'),
(103, 70, 'Tireo'),
(104, 70, 'Buena Vista'),
(105, 71, 'Manabao'),
(106, 72, 'Rincón'),
(107, 73, 'Arroyo al Medio'),
(108, 73, 'Las Gordas'),
(109, 73, 'San José de Matanzas'),
(110, 74, 'Arroyo Salado'),
(111, 74, 'La Entrada'),
(112, 75, 'El Pozo'),
(113, 77, 'Arroyo Toro-Masipedro'),
(114, 77, 'La Salvia-Los Quemados'),
(115, 77, 'Jayaco'),
(116, 77, 'Juma Bejucal'),
(117, 77, 'Sabana del Puerto'),
(118, 79, 'Juan Adrián'),
(119, 79, 'Villa Sonador'),
(120, 81, 'Palo Verde'),
(121, 82, 'Cana Chapetón'),
(122, 82, 'Hatillo Palma'),
(123, 82, 'Villa Elisa'),
(124, 86, 'Boyá'),
(125, 86, 'Chirino'),
(126, 86, 'Don Juan'),
(127, 89, 'Gonzalo'),
(128, 89, 'Majagual'),
(129, 90, 'Los Botados'),
(130, 91, 'José Francisco Peña Gómez'),
(131, 91, 'Juancho'),
(132, 93, 'Catalina'),
(133, 93, 'El Carretón'),
(134, 93, 'El Limonal'),
(135, 93, 'Las Barías'),
(136, 93, 'Matanzas'),
(137, 93, 'Paya'),
(138, 93, 'Sabana Buey'),
(139, 93, 'Villa Fundación'),
(140, 93, 'Villa Sombrero'),
(141, 94, 'Pizarrete'),
(142, 94, 'Santana'),
(143, 86, 'Maimón'),
(144, 86, 'Yásica Arriba'),
(145, 96, 'Río Grande'),
(146, 99, 'Navas'),
(147, 100, 'Belloso'),
(148, 100, 'Estrecho'),
(149, 100, 'La Isabela'),
(150, 101, 'Cabarete'),
(151, 101, 'Sabaneta de Yásica'),
(152, 102, 'Estero Hondo'),
(153, 102, 'Gualete'),
(154, 102, 'La Jaiba'),
(155, 104, 'Arroyo Barril'),
(156, 104, 'El Limón'),
(157, 104, 'Las Galeras'),
(158, 107, 'Hato Damas'),
(159, 108, 'El Carril'),
(160, 109, 'Cambita El Pueblecito'),
(161, 113, 'La Cuchilla'),
(162, 113, 'Medina'),
(163, 113, 'San José del Puerto'),
(164, 115, 'El Naranjal'),
(165, 115, 'El Pinar'),
(166, 115, 'La Ciénaga'),
(167, 115, 'Nizao-Las Auyamas'),
(168, 118, 'El Rosario'),
(169, 118, 'Guanito'),
(170, 118, 'Hato del Padre'),
(171, 118, 'Hato Nuevo'),
(172, 118, 'La Jagua'),
(173, 118, 'Las Charcas de María Nova'),
(174, 118, 'Pedro Corto'),
(175, 118, 'Sabana Alta'),
(176, 118, 'Sabaneta'),
(177, 119, 'Arroyo Cano'),
(178, 119, 'Yaque'),
(179, 120, 'Batista'),
(180, 120, 'Derrumbadero'),
(181, 121, 'Jínova'),
(182, 122, 'Carrera de Yegua'),
(183, 122, 'Matayaya'),
(184, 123, 'Jorjillo'),
(185, 129, 'El Puerto'),
(186, 129, 'Gautier'),
(187, 130, 'Caballero'),
(188, 130, 'Comedero Arriba'),
(189, 130, 'Quita Sueño'),
(190, 131, 'La Cueva'),
(191, 131, 'Platanal'),
(192, 133, 'Angelina'),
(193, 133, 'La Bija'),
(194, 133, 'Hernando Alonzo'),
(195, 134, 'Baitoa'),
(196, 134, 'Hato del Yaque'),
(197, 134, 'La Canela'),
(198, 134, 'Pedro García'),
(199, 134, 'San Francisco de Jacagua'),
(200, 136, 'El Caimito'),
(201, 136, 'Juncalito'),
(202, 137, 'Las Palomas'),
(203, 137, 'Canabacoa'),
(204, 137, 'Guayabal'),
(205, 140, 'El Rubio'),
(206, 140, 'La Cuesta'),
(207, 140, 'Las Placetas'),
(208, 141, 'Canca La Piedra'),
(209, 142, 'El Limón'),
(210, 142, 'Palmar Arriba'),
(211, 146, 'San Luis'),
(212, 147, 'La Caleta'),
(213, 148, 'Palmarejo-Villa Linda'),
(214, 148, 'Pantoja'),
(215, 149, 'La Cuaba'),
(216, 149, 'La Guáyiga'),
(217, 150, 'Hato Viejo'),
(218, 151, 'La Victoria'),
(219, 153, 'Ámina'),
(220, 153, 'Guatapanal'),
(221, 153, 'Jaibón (Pueblo Nuevo)'),
(222, 154, 'Boca de Mao'),
(223, 154, 'Jicomé'),
(224, 154, 'Maizal'),
(225, 154, 'Paradero'),
(226, 155, 'Cruce de Guayacanes'),
(227, 155, 'Jaibón'),
(228, 155, 'La Caya');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_civil`
--

CREATE TABLE `estado_civil` (
  `id` int(11) UNSIGNED NOT NULL,
  `descripcion` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Truncar tablas antes de insertar `estado_civil`
--

TRUNCATE TABLE `estado_civil`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipios`
--

CREATE TABLE `municipios` (
  `municipio_id` int(11) NOT NULL,
  `provincia_id` int(11) DEFAULT NULL,
  `nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Truncar tablas antes de insertar `municipios`
--

TRUNCATE TABLE `municipios`;
--
-- Volcado de datos para la tabla `municipios`
--

INSERT INTO `municipios` (`municipio_id`, `provincia_id`, `nombre`) VALUES
(1, 5, 'Distrito Nacional'),
(2, 1, 'Azua de Compostela'),
(3, 1, 'Estebanía'),
(4, 1, 'Guayabal'),
(5, 1, 'Las Charcas'),
(6, 1, 'Las Yayas de Viajama'),
(7, 1, 'Padre Las Casas'),
(8, 1, 'Peralta'),
(9, 1, 'Pueblo Viejo'),
(10, 1, 'Sabana Yegua'),
(11, 1, 'Tábara Arriba'),
(12, 2, 'Neiba'),
(13, 2, 'Galván'),
(14, 2, 'Los Ríos'),
(15, 2, 'Tamayo'),
(16, 2, 'Villa Jaragua'),
(17, 3, 'Barahona'),
(18, 3, 'Cabral'),
(19, 3, 'El Peñón'),
(20, 3, 'Enriquillo'),
(21, 3, 'Fundación'),
(22, 3, 'Jaquimeyes'),
(23, 3, 'La Ciénaga'),
(24, 3, 'Las Salinas'),
(25, 3, 'Paraíso'),
(26, 3, 'Polo'),
(27, 3, 'Vicente Noble'),
(28, 4, 'Dajabón'),
(29, 4, 'El Pino'),
(30, 4, 'Loma de Cabrera'),
(31, 4, 'Partido'),
(32, 4, 'Restauración'),
(33, 6, 'San Francisco de Macorís'),
(34, 6, 'Arenoso'),
(35, 6, 'Castillo'),
(36, 6, 'Eugenio María de Hostos'),
(37, 6, 'Las Guáranas'),
(38, 6, 'Pimentel'),
(39, 6, 'Villa Riva'),
(40, 8, 'El Seibo'),
(41, 8, 'Miches'),
(42, 7, 'Comendador'),
(43, 7, 'Bánica'),
(44, 7, 'El Llano'),
(45, 7, 'Hondo Valle'),
(46, 7, 'Juan Santiago'),
(47, 7, 'Pedro Santana'),
(48, 9, 'Moca'),
(49, 9, 'Cayetano Germosén'),
(50, 9, 'Gaspar Hernández'),
(51, 9, 'Jamao al Norte'),
(52, 10, 'Hato Mayor del Rey'),
(53, 10, 'El Valle'),
(54, 10, 'Sabana de la Mar'),
(55, 11, 'Salcedo'),
(56, 11, 'Tenares'),
(57, 11, 'Villa Tapia'),
(58, 12, 'Jimaní'),
(59, 12, 'Cristóbal'),
(60, 12, 'Duvergé'),
(61, 12, 'La Descubierta'),
(62, 12, 'Mella'),
(63, 12, 'Postrer Río'),
(64, 13, 'Higüey'),
(65, 13, 'San Rafael del Yuma'),
(66, 14, 'La Romana'),
(67, 14, 'Guaymate'),
(68, 14, 'Villa Hermosa'),
(69, 15, 'La Concepción de La Vega'),
(70, 15, 'Constanza'),
(71, 15, 'Jarabacoa'),
(72, 15, 'Jima Abajo'),
(73, 16, 'Nagua'),
(74, 16, 'Cabrera'),
(75, 16, 'El Factor'),
(76, 16, 'Río San Juan'),
(77, 17, 'Bonao'),
(78, 17, 'Maimón'),
(79, 17, 'Piedra Blanca'),
(80, 18, 'Montecristi'),
(81, 18, 'Castañuela'),
(82, 18, 'Guayubín'),
(83, 18, 'Las Matas de Santa Cruz'),
(84, 18, 'Pepillo Salcedo'),
(85, 18, 'Villa Vásquez'),
(86, 19, 'Monte Plata'),
(87, 19, 'Bayaguana'),
(88, 19, 'Peralvillo'),
(89, 19, 'Sabana Grande de Boyá'),
(90, 19, 'Yamasá'),
(91, 20, 'Pedernales'),
(92, 20, 'Oviedo'),
(93, 21, 'Baní'),
(94, 21, 'Nizao'),
(95, 22, 'Puerto Plata'),
(96, 22, 'Altamira'),
(97, 22, 'Guananico'),
(98, 22, 'Imbert'),
(99, 22, 'Los Hidalgos'),
(100, 22, 'Luperón'),
(101, 22, 'Sosúa'),
(102, 22, 'Villa Isabela'),
(103, 22, 'Villa Montellano'),
(104, 23, 'Samaná'),
(105, 23, 'Las Terrenas'),
(106, 23, 'Sánchez'),
(107, 25, 'San Cristóbal'),
(108, 25, 'Bajos de Haina'),
(109, 25, 'Cambita Garabito'),
(110, 25, 'Los Cacaos'),
(111, 25, 'Sabana Grande de Palenque'),
(112, 25, 'San Gregorio de Nigua'),
(113, 25, 'Villa Altagracia'),
(114, 25, 'Yaguate'),
(115, 26, 'San José de Ocoa'),
(116, 26, 'Rancho Arriba'),
(117, 26, 'Sabana Larga'),
(118, 27, 'San Juan de la Maguana'),
(119, 27, 'Bohechío'),
(120, 27, 'El Cercado'),
(121, 27, 'Juan de Herrera'),
(122, 27, 'Las Matas de Farfán'),
(123, 27, 'Vallejuelo'),
(124, 28, 'San Pedro de Macorís'),
(125, 28, 'Consuelo'),
(126, 28, 'Guayacanes'),
(127, 28, 'Quisqueya'),
(128, 28, 'Ramón Santana'),
(129, 28, 'San José de Los Llanos'),
(130, 24, 'Cotuí'),
(131, 24, 'Cevicos'),
(132, 24, 'Fantino'),
(133, 24, 'La Mata'),
(134, 29, 'Santiago'),
(135, 29, 'Bisonó'),
(136, 29, 'Jánico'),
(137, 29, 'Licey al Medio'),
(138, 29, 'Puñal'),
(139, 29, 'Sabana Iglesia'),
(140, 29, 'San José de las Matas'),
(141, 29, 'Tamboril'),
(142, 29, 'Villa González'),
(143, 30, 'San Ignacio de Sabaneta'),
(144, 30, 'Los Almácigos'),
(145, 30, 'Monción'),
(146, 31, 'Santo Domingo Este'),
(147, 31, 'Boca Chica'),
(148, 31, 'Los Alcarrizos'),
(149, 31, 'Pedro Brand'),
(150, 31, 'San Antonio de Guerra'),
(151, 31, 'Santo Domingo Norte'),
(152, 31, 'Santo Domingo Oeste'),
(153, 32, 'Mao'),
(154, 32, 'Esperanza'),
(155, 32, 'Laguna Salada');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `newsletters`
--

CREATE TABLE `newsletters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncar tablas antes de insertar `newsletters`
--

TRUNCATE TABLE `newsletters`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paises`
--

CREATE TABLE `paises` (
  `id` smallint(6) NOT NULL,
  `iso2` char(2) NOT NULL,
  `iso3` char(3) NOT NULL,
  `prefijo` smallint(6) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `continente` varchar(16) DEFAULT NULL,
  `subcontinente` varchar(32) DEFAULT NULL,
  `iso_moneda` varchar(3) DEFAULT NULL,
  `nombre_moneda` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Truncar tablas antes de insertar `paises`
--

TRUNCATE TABLE `paises`;
--
-- Volcado de datos para la tabla `paises`
--

INSERT INTO `paises` (`id`, `iso2`, `iso3`, `prefijo`, `nombre`, `continente`, `subcontinente`, `iso_moneda`, `nombre_moneda`) VALUES
(4, 'AF', 'AFG', 93, 'Afganistán', 'Asia', NULL, 'AFN', 'Afgani afgano'),
(8, 'AL', 'ALB', 355, 'Albania', 'Europa', NULL, 'ALL', 'Lek albanés'),
(10, 'AQ', 'ATA', 672, 'Antártida', 'Antártida', NULL, NULL, NULL),
(12, 'DZ', 'DZA', 213, 'Argelia', 'África', NULL, 'DZD', 'Dinar algerino'),
(16, 'AS', 'ASM', 1684, 'Samoa Americana', 'Oceanía', NULL, NULL, NULL),
(20, 'AD', 'AND', 376, 'Andorra', 'Europa', NULL, 'EUR', 'Euro'),
(24, 'AO', 'AGO', 244, 'Angola', 'África', NULL, 'AOA', 'Kwanza angoleño'),
(28, 'AG', 'ATG', 1268, 'Antigua y Barbuda', 'América', 'El Caribe', NULL, NULL),
(31, 'AZ', 'AZE', 994, 'Azerbaiyán', 'Asia', NULL, 'AZM', 'Manat azerbaiyano'),
(32, 'AR', 'ARG', 54, 'Argentina', 'América', 'América del Sur', 'ARS', 'Peso argentino'),
(36, 'AU', 'AUS', 61, 'Australia', 'Oceanía', NULL, 'AUD', 'Dólar australiano'),
(40, 'AT', 'AUT', 43, 'Austria', 'Europa', NULL, 'EUR', 'Euro'),
(44, 'BS', 'BHS', 1242, 'Bahamas', 'América', 'El Caribe', 'BSD', 'Dólar bahameño'),
(48, 'BH', 'BHR', 973, 'Bahréin', 'Asia', NULL, 'BHD', 'Dinar bahreiní'),
(50, 'BD', 'BGD', 880, 'Bangladesh', 'Asia', NULL, 'BDT', 'Taka de Bangladesh'),
(51, 'AM', 'ARM', 374, 'Armenia', 'Asia', NULL, 'AMD', 'Dram armenio'),
(52, 'BB', 'BRB', 1246, 'Barbados', 'América', 'El Caribe', 'BBD', 'Dólar de Barbados'),
(56, 'BE', 'BEL', 32, 'Bélgica', 'Europa', NULL, 'EUR', 'Euro'),
(60, 'BM', 'BMU', 1441, 'Bermudas', 'América', 'El Caribe', 'BMD', 'Dólar de Bermuda'),
(64, 'BT', 'BTN', 975, 'Bhután', 'Asia', NULL, 'BTN', 'Ngultrum de Bután'),
(68, 'BO', 'BOL', 591, 'Bolivia', 'América', 'América del Sur', 'BOB', 'Boliviano'),
(70, 'BA', 'BIH', 387, 'Bosnia y Herzegovina', 'Europa', NULL, 'BAM', 'Marco convertible de Bosnia-Herzegovina'),
(72, 'BW', 'BWA', 267, 'Botsuana', 'África', NULL, 'BWP', 'Pula de Botsuana'),
(74, 'BV', 'BVT', 0, 'Isla Bouvet', NULL, NULL, NULL, NULL),
(76, 'BR', 'BRA', 55, 'Brasil', 'América', 'América del Sur', 'BRL', 'Real brasileño'),
(84, 'BZ', 'BLZ', 501, 'Belice', 'América', 'América Central', 'BZD', 'Dólar de Belice'),
(86, 'IO', 'IOT', 0, 'Territorio Británico del Océano Índico', NULL, NULL, NULL, NULL),
(90, 'SB', 'SLB', 677, 'Islas Salomón', 'Oceanía', NULL, 'SBD', 'Dólar de las Islas Salomón'),
(92, 'VG', 'VGB', 1284, 'Islas Vírgenes Británicas', 'América', 'El Caribe', NULL, NULL),
(96, 'BN', 'BRN', 673, 'Brunéi', 'Asia', NULL, 'BND', 'Dólar de Brunéi'),
(100, 'BG', 'BGR', 359, 'Bulgaria', 'Europa', NULL, 'BGN', 'Lev búlgaro'),
(104, 'MM', 'MMR', 95, 'Myanmar', 'Asia', NULL, 'MMK', 'Kyat birmano'),
(108, 'BI', 'BDI', 257, 'Burundi', 'África', NULL, 'BIF', 'Franco burundés'),
(112, 'BY', 'BLR', 375, 'Bielorrusia', 'Europa', NULL, 'BYR', 'Rublo bielorruso'),
(116, 'KH', 'KHM', 855, 'Camboya', 'Asia', NULL, 'KHR', 'Riel camboyano'),
(120, 'CM', 'CMR', 237, 'Camerún', 'África', NULL, NULL, NULL),
(124, 'CA', 'CAN', 1, 'Canadá', 'América', 'América del Norte', 'CAD', 'Dólar canadiense'),
(132, 'CV', 'CPV', 238, 'Cabo Verde', 'África', NULL, 'CVE', 'Escudo caboverdiano'),
(136, 'KY', 'CYM', 1345, 'Islas Caimán', 'América', 'El Caribe', 'KYD', 'Dólar caimano (de Islas Caimán)'),
(140, 'CF', 'CAF', 236, 'República Centroafricana', 'África', NULL, NULL, NULL),
(144, 'LK', 'LKA', 94, 'Sri Lanka', 'Asia', NULL, 'LKR', 'Rupia de Sri Lanka'),
(148, 'TD', 'TCD', 235, 'Chad', 'África', NULL, NULL, NULL),
(152, 'CL', 'CHL', 56, 'Chile', 'América', 'América del Sur', 'CLP', 'Peso chileno'),
(156, 'CN', 'CHN', 86, 'China', 'Asia', NULL, 'CNY', 'Yuan Renminbi de China'),
(158, 'TW', 'TWN', 886, 'Taiwán', 'Asia', NULL, 'TWD', 'Dólar taiwanés'),
(162, 'CX', 'CXR', 61, 'Isla de Navidad', 'Oceanía', NULL, NULL, NULL),
(166, 'CC', 'CCK', 61, 'Islas Cocos', 'Óceanía', NULL, NULL, NULL),
(170, 'CO', 'COL', 57, 'Colombia', 'América', 'América del Sur', 'COP', 'Peso colombiano'),
(174, 'KM', 'COM', 269, 'Comoras', 'África', NULL, 'KMF', 'Franco comoriano (de Comoras)'),
(175, 'YT', 'MYT', 262, 'Mayotte', 'África', NULL, NULL, NULL),
(178, 'CG', 'COG', 242, 'Congo', 'África', NULL, NULL, NULL),
(180, 'CD', 'COD', 243, 'República Democrática del Congo', 'África', NULL, 'CDF', 'Franco congoleño'),
(184, 'CK', 'COK', 682, 'Islas Cook', 'Oceanía', NULL, NULL, NULL),
(188, 'CR', 'CRI', 506, 'Costa Rica', 'América', 'América Central', 'CRC', 'Colón costarricense'),
(191, 'HR', 'HRV', 385, 'Croacia', 'Europa', NULL, 'HRK', 'Kuna croata'),
(192, 'CU', 'CUB', 53, 'Cuba', 'América', 'El Caribe', 'CUP', 'Peso cubano'),
(196, 'CY', 'CYP', 357, 'Chipre', 'Europa', NULL, 'CYP', 'Libra chipriota'),
(203, 'CZ', 'CZE', 420, 'República Checa', 'Europa', NULL, 'CZK', 'Koruna checa'),
(204, 'BJ', 'BEN', 229, 'Benín', 'África', NULL, NULL, NULL),
(208, 'DK', 'DNK', 45, 'Dinamarca', 'Europa', NULL, 'DKK', 'Corona danesa'),
(212, 'DM', 'DMA', 1767, 'Dominica', 'América', 'El Caribe', NULL, NULL),
(214, 'DO', 'DOM', 1809, 'República Dominicana', 'América', 'El Caribe', 'DOP', 'Peso dominicano'),
(218, 'EC', 'ECU', 593, 'Ecuador', 'América', 'América del Sur', NULL, NULL),
(222, 'SV', 'SLV', 503, 'El Salvador', 'América', 'América Central', 'SVC', 'Colón salvadoreño'),
(226, 'GQ', 'GNQ', 240, 'Guinea Ecuatorial', 'África', NULL, NULL, NULL),
(231, 'ET', 'ETH', 251, 'Etiopía', 'África', NULL, 'ETB', 'Birr etíope'),
(232, 'ER', 'ERI', 291, 'Eritrea', 'África', NULL, 'ERN', 'Nakfa eritreo'),
(233, 'EE', 'EST', 372, 'Estonia', 'Europa', NULL, 'EEK', 'Corona estonia'),
(234, 'FO', 'FRO', 298, 'Islas Feroe', 'Europa', NULL, NULL, NULL),
(238, 'FK', 'FLK', 500, 'Islas Malvinas', 'América', 'América del Sur', 'FKP', 'Libra malvinense'),
(239, 'GS', 'SGS', 0, 'Islas Georgias del Sur y Sandwich del Sur', 'América', 'América del Sur', NULL, NULL),
(242, 'FJ', 'FJI', 679, 'Fiyi', 'Oceanía', NULL, 'FJD', 'Dólar fijiano'),
(246, 'FI', 'FIN', 358, 'Finlandia', 'Europa', NULL, 'EUR', 'Euro'),
(248, 'AX', 'ALA', 0, 'Islas Gland', 'Europa', NULL, NULL, NULL),
(250, 'FR', 'FRA', 33, 'Francia', 'Europa', NULL, 'EUR', 'Euro'),
(254, 'GF', 'GUF', 0, 'Guayana Francesa', 'América', 'América del Sur', NULL, NULL),
(258, 'PF', 'PYF', 689, 'Polinesia Francesa', 'Oceanía', NULL, NULL, NULL),
(260, 'TF', 'ATF', 0, 'Territorios Australes Franceses', NULL, NULL, NULL, NULL),
(262, 'DJ', 'DJI', 253, 'Yibuti', 'África', NULL, 'DJF', 'Franco yibutiano'),
(266, 'GA', 'GAB', 241, 'Gabón', 'África', NULL, NULL, NULL),
(268, 'GE', 'GEO', 995, 'Georgia', 'Europa', NULL, 'GEL', 'Lari georgiano'),
(270, 'GM', 'GMB', 220, 'Gambia', 'África', NULL, 'GMD', 'Dalasi gambiano'),
(275, 'PS', 'PSE', 0, 'Palestina', 'Asia', NULL, NULL, NULL),
(276, 'DE', 'DEU', 49, 'Alemania', 'Europa', NULL, 'EUR', 'Euro'),
(288, 'GH', 'GHA', 233, 'Ghana', 'África', NULL, 'GHC', 'Cedi ghanés'),
(292, 'GI', 'GIB', 350, 'Gibraltar', 'Europa', NULL, 'GIP', 'Libra de Gibraltar'),
(296, 'KI', 'KIR', 686, 'Kiribati', 'Oceanía', NULL, NULL, NULL),
(300, 'GR', 'GRC', 30, 'Grecia', 'Europa', NULL, 'EUR', 'Euro'),
(304, 'GL', 'GRL', 299, 'Groenlandia', 'América', 'América del Norte', NULL, NULL),
(308, 'GD', 'GRD', 1473, 'Granada', 'América', 'El Caribe', NULL, NULL),
(312, 'GP', 'GLP', 0, 'Guadalupe', 'América', 'El Caribe', NULL, NULL),
(316, 'GU', 'GUM', 1671, 'Guam', 'Oceanía', NULL, NULL, NULL),
(320, 'GT', 'GTM', 502, 'Guatemala', 'América', 'América Central', 'GTQ', 'Quetzal guatemalteco'),
(324, 'GN', 'GIN', 224, 'Guinea', 'África', NULL, 'GNF', 'Franco guineano'),
(328, 'GY', 'GUY', 592, 'Guyana', 'América', 'América del Sur', 'GYD', 'Dólar guyanés'),
(332, 'HT', 'HTI', 509, 'Haití', 'América', 'El Caribe', 'HTG', 'Gourde haitiano'),
(334, 'HM', 'HMD', 0, 'Islas Heard y McDonald', 'Oceanía', NULL, NULL, NULL),
(336, 'VA', 'VAT', 39, 'Ciudad del Vaticano', 'Europa', NULL, NULL, NULL),
(340, 'HN', 'HND', 504, 'Honduras', 'América', 'América Central', 'HNL', 'Lempira hondureño'),
(344, 'HK', 'HKG', 852, 'Hong Kong', 'Asia', NULL, 'HKD', 'Dólar de Hong Kong'),
(348, 'HU', 'HUN', 36, 'Hungría', 'Europa', NULL, 'HUF', 'Forint húngaro'),
(352, 'IS', 'ISL', 354, 'Islandia', 'Europa', NULL, 'ISK', 'Króna islandesa'),
(356, 'IN', 'IND', 91, 'India', 'Asia', NULL, 'INR', 'Rupia india'),
(360, 'ID', 'IDN', 62, 'Indonesia', 'Asia', NULL, 'IDR', 'Rupiah indonesia'),
(364, 'IR', 'IRN', 98, 'Irán', 'Asia', NULL, 'IRR', 'Rial iraní'),
(368, 'IQ', 'IRQ', 964, 'Iraq', 'Asia', NULL, 'IQD', 'Dinar iraquí'),
(372, 'IE', 'IRL', 353, 'Irlanda', 'Europa', NULL, 'EUR', 'Euro'),
(376, 'IL', 'ISR', 972, 'Israel', 'Asia', NULL, 'ILS', 'Nuevo shéquel israelí'),
(380, 'IT', 'ITA', 39, 'Italia', 'Europa', NULL, 'EUR', 'Euro'),
(384, 'CI', 'CIV', 225, 'Costa de Marfil', 'África', NULL, NULL, NULL),
(388, 'JM', 'JAM', 1876, 'Jamaica', 'América', 'El Caribe', 'JMD', 'Dólar jamaicano'),
(392, 'JP', 'JPN', 81, 'Japón', 'Asia', NULL, 'JPY', 'Yen japonés'),
(398, 'KZ', 'KAZ', 7, 'Kazajstán', 'Asia', NULL, 'KZT', 'Tenge kazajo'),
(400, 'JO', 'JOR', 962, 'Jordania', 'Asia', NULL, 'JOD', 'Dinar jordano'),
(404, 'KE', 'KEN', 254, 'Kenia', 'África', NULL, 'KES', 'Chelín keniata'),
(408, 'KP', 'PRK', 850, 'Corea del Norte', 'Asia', NULL, 'KPW', 'Won norcoreano'),
(410, 'KR', 'KOR', 82, 'Corea del Sur', 'Asia', NULL, 'KRW', 'Won surcoreano'),
(414, 'KW', 'KWT', 965, 'Kuwait', 'Asia', NULL, 'KWD', 'Dinar kuwaití'),
(417, 'KG', 'KGZ', 996, 'Kirguistán', 'Asia', NULL, 'KGS', 'Som kirguís (de Kirguistán)'),
(418, 'LA', 'LAO', 856, 'Laos', 'Asia', NULL, 'LAK', 'Kip lao'),
(422, 'LB', 'LBN', 961, 'Líbano', 'Asia', NULL, 'LBP', 'Libra libanesa'),
(426, 'LS', 'LSO', 266, 'Lesotho', 'África', NULL, 'LSL', 'Loti lesotense'),
(428, 'LV', 'LVA', 371, 'Letonia', 'Europa', NULL, 'LVL', 'Lat letón'),
(430, 'LR', 'LBR', 231, 'Liberia', 'África', NULL, 'LRD', 'Dólar liberiano'),
(434, 'LY', 'LBY', 218, 'Libia', 'África', NULL, 'LYD', 'Dinar libio'),
(438, 'LI', 'LIE', 423, 'Liechtenstein', 'Europa', NULL, NULL, NULL),
(440, 'LT', 'LTU', 370, 'Lituania', 'Europa', NULL, 'LTL', 'Litas lituano'),
(442, 'LU', 'LUX', 352, 'Luxemburgo', 'Europa', NULL, 'EUR', 'Euro'),
(446, 'MO', 'MAC', 853, 'Macao', 'Asia', NULL, 'MOP', 'Pataca de Macao'),
(450, 'MG', 'MDG', 261, 'Madagascar', 'África', NULL, 'MGA', 'Ariary malgache'),
(454, 'MW', 'MWI', 265, 'Malaui', 'África', NULL, 'MWK', 'Kwacha malauiano'),
(458, 'MY', 'MYS', 60, 'Malasia', 'Asia', NULL, 'MYR', 'Ringgit malayo'),
(462, 'MV', 'MDV', 960, 'Maldivas', 'Asia', NULL, 'MVR', 'Rufiyaa maldiva'),
(466, 'ML', 'MLI', 223, 'Malí', 'África', NULL, NULL, NULL),
(470, 'MT', 'MLT', 356, 'Malta', 'Europa', NULL, 'MTL', 'Lira maltesa'),
(474, 'MQ', 'MTQ', 0, 'Martinica', 'América', 'El Caribe', NULL, NULL),
(478, 'MR', 'MRT', 222, 'Mauritania', 'África', NULL, 'MRO', 'Ouguiya mauritana'),
(480, 'MU', 'MUS', 230, 'Mauricio', 'África', NULL, 'MUR', 'Rupia mauricia'),
(484, 'MX', 'MEX', 52, 'México', 'América', 'América del Norte', 'MXN', 'Peso mexicano'),
(492, 'MC', 'MCO', 377, 'Mónaco', 'Europa', NULL, NULL, NULL),
(496, 'MN', 'MNG', 976, 'Mongolia', 'Asia', NULL, 'MNT', 'Tughrik mongol'),
(498, 'MD', 'MDA', 373, 'Moldavia', 'Europa', NULL, 'MDL', 'Leu moldavo'),
(499, 'ME', 'MNE', 382, 'Montenegro', 'Europa', NULL, NULL, NULL),
(500, 'MS', 'MSR', 1664, 'Montserrat', 'América', 'El Caribe', NULL, NULL),
(504, 'MA', 'MAR', 212, 'Marruecos', 'África', NULL, 'MAD', 'Dirham marroquí'),
(508, 'MZ', 'MOZ', 258, 'Mozambique', 'África', NULL, 'MZM', 'Metical mozambiqueño'),
(512, 'OM', 'OMN', 968, 'Omán', 'Asia', NULL, 'OMR', 'Rial omaní'),
(516, 'NA', 'NAM', 264, 'Namibia', 'África', NULL, 'NAD', 'Dólar namibio'),
(520, 'NR', 'NRU', 674, 'Nauru', 'Oceanía', NULL, NULL, NULL),
(524, 'NP', 'NPL', 977, 'Nepal', 'Asia', NULL, 'NPR', 'Rupia nepalesa'),
(528, 'NL', 'NLD', 31, 'Países Bajos', 'Europa', NULL, 'EUR', 'Euro'),
(530, 'AN', 'ANT', 599, 'Antillas Holandesas', 'América', 'El Caribe', 'ANG', 'Florín antillano neerlandés'),
(533, 'AW', 'ABW', 297, 'Aruba', 'América', 'El Caribe', 'AWG', 'Florín arubeño'),
(540, 'NC', 'NCL', 687, 'Nueva Caledonia', 'Oceanía', NULL, NULL, NULL),
(548, 'VU', 'VUT', 678, 'Vanuatu', 'Oceanía', NULL, 'VUV', 'Vatu vanuatense'),
(554, 'NZ', 'NZL', 64, 'Nueva Zelanda', 'Oceanía', NULL, 'NZD', 'Dólar neozelandés'),
(558, 'NI', 'NIC', 505, 'Nicaragua', 'América', 'América Central', 'NIO', 'Córdoba nicaragüense'),
(562, 'NE', 'NER', 227, 'Níger', 'África', NULL, NULL, NULL),
(566, 'NG', 'NGA', 234, 'Nigeria', 'África', NULL, 'NGN', 'Naira nigeriana'),
(570, 'NU', 'NIU', 683, 'Niue', 'Oceanía', NULL, NULL, NULL),
(574, 'NF', 'NFK', 0, 'Isla Norfolk', 'Oceanía', NULL, NULL, NULL),
(578, 'NO', 'NOR', 47, 'Noruega', 'Europa', NULL, 'NOK', 'Corona noruega'),
(580, 'MP', 'MNP', 1670, 'Islas Marianas del Norte', 'Oceanía', NULL, NULL, NULL),
(581, 'UM', 'UMI', 0, 'Islas Ultramarinas de Estados Unidos', NULL, NULL, NULL, NULL),
(583, 'FM', 'FSM', 691, 'Micronesia', 'Oceanía', NULL, NULL, NULL),
(584, 'MH', 'MHL', 692, 'Islas Marshall', 'Oceanía', NULL, NULL, NULL),
(585, 'PW', 'PLW', 680, 'Palaos', 'Oceanía', NULL, NULL, NULL),
(586, 'PK', 'PAK', 92, 'Pakistán', 'Asia', NULL, 'PKR', 'Rupia pakistaní'),
(591, 'PA', 'PAN', 507, 'Panamá', 'América', 'América Central', 'PAB', 'Balboa panameña'),
(598, 'PG', 'PNG', 675, 'Papúa Nueva Guinea', 'Oceanía', NULL, 'PGK', 'Kina de Papúa Nueva Guinea'),
(600, 'PY', 'PRY', 595, 'Paraguay', 'América', 'América del Sur', 'PYG', 'Guaraní paraguayo'),
(604, 'PE', 'PER', 51, 'Perú', 'América', 'América del Sur', 'PEN', 'Nuevo sol peruano'),
(608, 'PH', 'PHL', 63, 'Filipinas', 'Asia', NULL, 'PHP', 'Peso filipino'),
(612, 'PN', 'PCN', 870, 'Islas Pitcairn', 'Oceanía', NULL, NULL, NULL),
(616, 'PL', 'POL', 48, 'Polonia', 'Europa', NULL, 'PLN', 'zloty polaco'),
(620, 'PT', 'PRT', 351, 'Portugal', 'Europa', NULL, 'EUR', 'Euro'),
(624, 'GW', 'GNB', 245, 'Guinea-Bissau', 'África', NULL, NULL, NULL),
(626, 'TL', 'TLS', 670, 'Timor Oriental', 'Asia', NULL, NULL, NULL),
(630, 'PR', 'PRI', 1, 'Puerto Rico', 'América', 'El Caribe', NULL, NULL),
(634, 'QA', 'QAT', 974, 'Qatar', 'Asia', NULL, 'QAR', 'Rial qatarí'),
(638, 'RE', 'REU', 262, 'Reunión', 'África', NULL, NULL, NULL),
(642, 'RO', 'ROU', 40, 'Rumania', 'Europa', NULL, 'RON', 'Leu rumano'),
(643, 'RU', 'RUS', 7, 'Rusia', 'Asia', NULL, 'RUB', 'Rublo ruso'),
(646, 'RW', 'RWA', 250, 'Ruanda', 'África', NULL, 'RWF', 'Franco ruandés'),
(654, 'SH', 'SHN', 290, 'Santa Helena', 'África', NULL, 'SHP', 'Libra de Santa Helena'),
(659, 'KN', 'KNA', 1869, 'San Cristóbal y Nieves', 'América', 'El Caribe', NULL, NULL),
(660, 'AI', 'AIA', 1264, 'Anguila', 'América', 'El Caribe', NULL, NULL),
(662, 'LC', 'LCA', 1758, 'Santa Lucía', 'América', 'El Caribe', NULL, NULL),
(666, 'PM', 'SPM', 508, 'San Pedro y Miquelón', 'América', 'América del Norte', NULL, NULL),
(670, 'VC', 'VCT', 1784, 'San Vicente y las Granadinas', 'América', 'El Caribe', NULL, NULL),
(674, 'SM', 'SMR', 378, 'San Marino', 'Europa', NULL, NULL, NULL),
(678, 'ST', 'STP', 239, 'Santo Tomé y Príncipe', 'África', NULL, 'STD', 'Dobra de Santo Tomé y Príncipe'),
(682, 'SA', 'SAU', 966, 'Arabia Saudí', 'Asia', NULL, 'SAR', 'Riyal saudí'),
(686, 'SN', 'SEN', 221, 'Senegal', 'África', NULL, NULL, NULL),
(688, 'RS', 'SRB', 381, 'Serbia', 'Europa', NULL, NULL, NULL),
(690, 'SC', 'SYC', 248, 'Seychelles', 'África', NULL, 'SCR', 'Rupia de Seychelles'),
(694, 'SL', 'SLE', 232, 'Sierra Leona', 'África', NULL, 'SLL', 'Leone de Sierra Leona'),
(702, 'SG', 'SGP', 65, 'Singapur', 'Asia', NULL, 'SGD', 'Dólar de Singapur'),
(703, 'SK', 'SVK', 421, 'Eslovaquia', 'Europa', NULL, 'SKK', 'Corona eslovaca'),
(704, 'VN', 'VNM', 84, 'Vietnam', 'Asia', NULL, 'VND', 'Dong vietnamita'),
(705, 'SI', 'SVN', 386, 'Eslovenia', 'Europa', NULL, NULL, NULL),
(706, 'SO', 'SOM', 252, 'Somalia', 'África', NULL, 'SOS', 'Chelín somalí'),
(710, 'ZA', 'ZAF', 27, 'Sudáfrica', 'África', NULL, 'ZAR', 'Rand sudafricano'),
(716, 'ZW', 'ZWE', 263, 'Zimbabue', 'África', NULL, 'ZWL', 'Dólar zimbabuense'),
(724, 'ES', 'ESP', 34, 'España', 'Europa', NULL, 'EUR', 'Euro'),
(732, 'EH', 'ESH', 0, 'Sahara Occidental', 'África', NULL, NULL, NULL),
(736, 'SD', 'SDN', 249, 'Sudán', 'África', NULL, 'SDD', 'Dinar sudanés'),
(740, 'SR', 'SUR', 597, 'Surinam', 'América', 'América del Sur', 'SRD', 'Dólar surinamés'),
(744, 'SJ', 'SJM', 0, 'Svalbard y Jan Mayen', 'Europa', NULL, NULL, NULL),
(748, 'SZ', 'SWZ', 268, 'Suazilandia', 'África', NULL, 'SZL', 'Lilangeni suazi'),
(752, 'SE', 'SWE', 46, 'Suecia', 'Europa', NULL, 'SEK', 'Corona sueca'),
(756, 'CH', 'CHE', 41, 'Suiza', 'Europa', NULL, 'CHF', 'Franco suizo'),
(760, 'SY', 'SYR', 963, 'Siria', 'Asia', NULL, 'SYP', 'Libra siria'),
(762, 'TJ', 'TJK', 992, 'Tayikistán', 'Asia', NULL, 'TJS', 'Somoni tayik (de Tayikistán)'),
(764, 'TH', 'THA', 66, 'Tailandia', 'Asia', NULL, 'THB', 'Baht tailandés'),
(768, 'TG', 'TGO', 228, 'Togo', 'África', NULL, NULL, NULL),
(772, 'TK', 'TKL', 690, 'Tokelau', 'Oceanía', NULL, NULL, NULL),
(776, 'TO', 'TON', 676, 'Tonga', 'Oceanía', NULL, 'TOP', 'Pa\'anga tongano'),
(780, 'TT', 'TTO', 1868, 'Trinidad y Tobago', 'América', 'El Caribe', 'TTD', 'Dólar de Trinidad y Tobago'),
(784, 'AE', 'ARE', 971, 'Emiratos Árabes Unidos', 'Asia', NULL, 'AED', 'Dirham de los Emiratos Árabes Unidos'),
(788, 'TN', 'TUN', 216, 'Túnez', 'África', NULL, 'TND', 'Dinar tunecino'),
(792, 'TR', 'TUR', 90, 'Turquía', 'Asia', NULL, 'TRY', 'Lira turca'),
(795, 'TM', 'TKM', 993, 'Turkmenistán', 'Asia', NULL, 'TMM', 'Manat turcomano'),
(796, 'TC', 'TCA', 1649, 'Islas Turcas y Caicos', 'América', 'El Caribe', NULL, NULL),
(798, 'TV', 'TUV', 688, 'Tuvalu', 'Oceanía', NULL, NULL, NULL),
(800, 'UG', 'UGA', 256, 'Uganda', 'África', NULL, 'UGX', 'Chelín ugandés'),
(804, 'UA', 'UKR', 380, 'Ucrania', 'Europa', NULL, 'UAH', 'Grivna ucraniana'),
(807, 'MK', 'MKD', 389, 'Macedonia', 'Europa', NULL, 'MKD', 'Denar macedonio'),
(818, 'EG', 'EGY', 20, 'Egipto', 'África', NULL, 'EGP', 'Libra egipcia'),
(826, 'GB', 'GBR', 44, 'Reino Unido', 'Europa', NULL, 'GBP', 'Libra esterlina (libra de Gran Bretaña)'),
(834, 'TZ', 'TZA', 255, 'Tanzania', 'África', NULL, 'TZS', 'Chelín tanzano'),
(840, 'US', 'USA', 1, 'Estados Unidos', 'América', 'América del Norte', 'USD', 'Dólar estadounidense'),
(850, 'VI', 'VIR', 1340, 'Islas Vírgenes de los Estados Unidos', 'América', 'El Caribe', NULL, NULL),
(854, 'BF', 'BFA', 226, 'Burkina Faso', 'África', NULL, NULL, NULL),
(858, 'UY', 'URY', 598, 'Uruguay', 'América', 'América del Sur', 'UYU', 'Peso uruguayo'),
(860, 'UZ', 'UZB', 998, 'Uzbekistán', 'Asia', NULL, 'UZS', 'Som uzbeko'),
(862, 'VE', 'VEN', 58, 'Venezuela', 'América', 'América del Sur', 'VEB', 'Bolívar venezolano'),
(876, 'WF', 'WLF', 681, 'Wallis y Futuna', 'Oceanía', NULL, NULL, NULL),
(882, 'WS', 'WSM', 685, 'Samoa', 'Oceanía', NULL, 'WST', 'Tala samoana'),
(887, 'YE', 'YEM', 967, 'Yemen', 'Asia', NULL, 'YER', 'Rial yemení (de Yemen)'),
(894, 'ZM', 'ZMB', 260, 'Zambia', 'África', NULL, 'ZMK', 'Kwacha zambiano');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncar tablas antes de insertar `permissions`
--

TRUNCATE TABLE `permissions`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `post_tag`
--

CREATE TABLE `post_tag` (
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `tag_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncar tablas antes de insertar `post_tag`
--

TRUNCATE TABLE `post_tag`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profiles`
--

CREATE TABLE `profiles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `pais_id` int(11) DEFAULT NULL,
  `provincia_id` int(11) DEFAULT NULL,
  `municipio_id` int(11) DEFAULT NULL,
  `distrito_id` int(11) DEFAULT NULL,
  `nacionalidad_id` int(11) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `estado_civil_id` int(11) UNSIGNED DEFAULT NULL,
  `sexo_id` int(11) UNSIGNED DEFAULT NULL,
  `tipo_doc_id` int(11) UNSIGNED DEFAULT NULL,
  `religion_id` int(11) UNSIGNED DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `avatar_url` varchar(255) DEFAULT NULL,
  `documento` varchar(50) DEFAULT NULL,
  `fecha_nac` date DEFAULT NULL,
  `lugar_nac` varchar(100) DEFAULT NULL,
  `direccion` varchar(200) DEFAULT NULL,
  `localidad` varchar(100) DEFAULT NULL,
  `codigo_postal` varchar(50) DEFAULT NULL,
  `biografia` varchar(255) DEFAULT NULL,
  `visibility` tinyint(1) DEFAULT 0,
  `ipv4` varchar(255) DEFAULT NULL,
  `sitio_web` varchar(255) DEFAULT NULL,
  `twitter_username` varchar(255) DEFAULT NULL,
  `facebook_username` varchar(255) DEFAULT NULL,
  `github_username` varchar(255) DEFAULT NULL,
  `instagram_username` varchar(255) DEFAULT NULL,
  `linkedin_username` varchar(255) DEFAULT NULL,
  `user_profile_bg` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncar tablas antes de insertar `profiles`
--

TRUNCATE TABLE `profiles`;
--
-- Volcado de datos para la tabla `profiles`
--

INSERT INTO `profiles` (`id`, `user_id`, `pais_id`, `provincia_id`, `municipio_id`, `distrito_id`, `nacionalidad_id`, `telephone`, `estado_civil_id`, `sexo_id`, `tipo_doc_id`, `religion_id`, `location`, `avatar_url`, `documento`, `fecha_nac`, `lugar_nac`, `direccion`, `localidad`, `codigo_postal`, `biografia`, `visibility`, `ipv4`, `sitio_web`, `twitter_username`, `facebook_username`, `github_username`, `instagram_username`, `linkedin_username`, `user_profile_bg`) VALUES
(1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 17, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provincias`
--

CREATE TABLE `provincias` (
  `provincia_id` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Truncar tablas antes de insertar `provincias`
--

TRUNCATE TABLE `provincias`;
--
-- Volcado de datos para la tabla `provincias`
--

INSERT INTO `provincias` (`provincia_id`, `nombre`) VALUES
(1, 'Azua'),
(2, 'Bahoruco'),
(3, 'Barahona'),
(4, 'Dajabón'),
(5, 'Distrito Nacional'),
(6, 'Duarte'),
(7, 'Elías Piña'),
(8, 'El Seibo'),
(9, 'Espaillat'),
(10, 'Hato Mayor'),
(11, 'Hermanas Mirabal'),
(12, 'Independencia'),
(13, 'La Altagracia'),
(14, 'La Romana'),
(15, 'La Vega'),
(16, 'María Trinidad Sánchez'),
(17, 'Monseñor Nouel'),
(18, 'Monte Cristi'),
(19, 'Monte Plata'),
(20, 'Pedernales'),
(21, 'Peravia'),
(22, 'Puerto Plata'),
(23, 'Samaná'),
(24, 'Sánchez Ramírez'),
(25, 'San Cristóbal'),
(26, 'San José de Ocoa'),
(27, 'San Juan'),
(28, 'San Pedro de Macorís'),
(29, 'Santiago'),
(30, 'Santiago Rodríguez'),
(31, 'Santo Domingo'),
(32, 'Valverde');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `guard_name` varchar(20) NOT NULL DEFAULT 'web',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncar tablas antes de insertar `roles`
--

TRUNCATE TABLE `roles`;
--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrador del sistema: tiene acceso completo al sistema y puede realizar cualquier acción, como crear y eliminar cuentas de usuario, administrar permisos y configuraciones, y ver registros de actividad.', 'web', '2023-04-11 00:45:16', '2023-04-11 00:45:16'),
(2, 'author', 'Autor del blog: un usuario que tiene permisos para crear y publicar contenido en el blog.', 'web', '2023-04-11 00:45:16', '2023-04-11 00:45:16'),
(3, 'editor', 'Editor del blog: un usuario que tiene permisos para editar y publicar contenido creado por otros autores del blog.', 'web', '2023-04-11 00:46:47', '2023-04-11 00:46:47'),
(4, 'guest', 'Guest: un usuario que tiene permisos para comentar en las publicaciones del blog.', 'web', '2023-04-11 00:46:47', '2023-04-11 00:46:47'),
(5, 'moderator', 'Moderador de comentarios: un usuario que tiene permisos para aprobar o rechazar los comentarios de otros usuarios en el blog.', 'web', '2023-04-11 00:47:52', '2023-04-11 00:47:52'),
(6, 'suscriptor', 'Suscriptor: un usuario que tiene la opción de recibir actualizaciones de nuevas publicaciones en el blog.', 'web', '2023-04-11 00:47:52', '2023-04-11 00:47:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncar tablas antes de insertar `role_has_permissions`
--

TRUNCATE TABLE `role_has_permissions`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sexos`
--

CREATE TABLE `sexos` (
  `id` int(11) UNSIGNED NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Truncar tablas antes de insertar `sexos`
--

TRUNCATE TABLE `sexos`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tags`
--

CREATE TABLE `tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncar tablas antes de insertar `tags`
--

TRUNCATE TABLE `tags`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Truncar tablas antes de insertar `users`
--

TRUNCATE TABLE `users`;
--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `username`, `password`, `phone`, `avatar`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Jose', 'Alcantara', 'admin@admin.com', 'admin', '$2y$10$ss5ZCOfLJprUwB5CyMKZ4.eRWKbtRxgG19g0sm/INzDOQuMIbawrm', NULL, NULL, '2020-08-08 11:46:05', '2023-04-13 20:44:09', NULL),
(3, 'Oscar', 'Liriano', 'test@test.com', 'test', '$2y$10$7gy27M9yBNjzQkY.Aklo3.JVMkKZia9MAqmXH8zdKuSQwkz5UeOtm', NULL, NULL, '2020-08-08 12:38:59', '2023-04-13 20:44:30', NULL),
(5, 'Florentino', 'Baez', 'florentinobaez.h@gmail.com', 'florentz14', '$2y$12$TR2uuxovLldG1LEj0rRkPeCi382bnBjEMDrOurw8fw.ZUyyuETSyS', '829-419-7064', NULL, '2023-04-10 20:19:09', '2023-04-10 20:19:09', NULL),
(6, 'Victor', 'Morales', 'victor@test.com', 'victor14', '$2y$12$ilpPFc03R1j7Znu8yrzp4.ZoLYR9R2/MbWktbSdbG9Qyq0VMJ9g8q', '829-890-0000', NULL, '2023-04-10 20:26:50', '2023-04-10 20:27:41', NULL),
(7, 'Francisco', 'Mora', 'francisco@test.com', 'mora14', '$2y$12$0S3weSLkpB2/0JmEz8/EnOVBk2hQvxiNX4R6jo1q0qjZ5BZbMFeQ.', '829-555-5555', NULL, '2023-04-10 20:32:36', '2023-04-13 13:41:53', NULL),
(8, 'Rainel', 'Cuevas', 'rainel14@test.com', 'rainel14', '$2y$12$zivbq7mrbk4jbryJF1xA7eZhOjYjonkFElLgZ01TJKRIMOxaeNpGG', '555-555-5555', NULL, '2023-04-13 20:25:32', '2023-04-13 20:25:32', NULL),
(10, 'Jeirison', 'Volquez', 'jeirison14@test.com', 'jeirison14', '$2y$12$PBPWxe6Q6Y36Chb0TBmUr.uXNx5FUte0ZMVumrB.kNj2/gQQyCgOu', '555-555-5554', NULL, '2023-04-13 20:29:01', '2023-04-13 20:29:01', NULL),
(11, 'Cartel', 'Florentino', 'cartel14@test.com', 'cartel14', '$2y$12$JWmhiz5LLySLbq/AawEzt./gMlLCsJd0.RRoKrJvzOusFyfhw4xFe', '555-555-5557', NULL, '2023-04-13 20:30:15', '2023-04-13 20:30:15', NULL),
(12, 'Frank', 'Martinez', 'frank14@test.com', 'frank14', '$2y$12$wzPGI.cEyKZ7a7EFL2R9pe.Rgl.XJvWhzoU4YhFVXjw99R3km3Zl6', '555-555-5558', NULL, '2023-04-13 20:32:01', '2023-04-13 20:32:01', NULL),
(13, 'Jhon', 'Maiden', 'jhon14@test.com', 'jhon14', '$2y$12$iydMuSmA9KdNr95zFqLjW.OwPWv2ysuhvSQlpaqacsu3ZZPtnoZ/G', '555-555-5559', NULL, '2023-04-13 20:33:41', '2023-04-13 20:33:41', NULL),
(14, 'karla', 'Soriano', 'karla4@test.com', 'karla4', '$2y$12$afoN9epOp32rMq4RfkF5ve2UyK7ozCGG3A.W0bb.a22PYx4FjDnZ.', '555-555-5551', NULL, '2023-04-13 20:35:48', '2023-04-13 20:35:48', NULL),
(15, 'Valeria', 'Soto', 'valeria14@test.com', 'valeria14', '$2y$12$F4jp8zv6aOGA.EeoJw5Hje7mNv60cRERAMAZf6TbKZlhbePLKrejS', '555-555-5552', NULL, '2023-04-13 20:36:53', '2023-04-13 20:36:53', NULL),
(16, 'Mario', 'Tomas', 'mario14@test.com', 'mario14', '$2y$12$xA2NqqorhYU3gFSlw9w9xOkEV.f6iF6YsvchFRn0yxplPXWXv6DEW', '555-555-5553', NULL, '2023-04-13 20:38:04', '2023-04-13 20:38:04', NULL),
(17, 'German', 'Trevin', 'german14@test.com', 'german14', '$2y$12$cijBTsK16eMvcId/79SBHOuR0SSWFQEM.37WkaV76oSwZ9et5TVhK', '555-888-5555', NULL, '2023-04-13 20:40:50', '2023-04-13 20:40:50', NULL),
(18, 'Pablo', 'Jolero', 'pablo14@test.com', 'pablo14', '$2y$12$aAaXMMEP5hQPmd5lQsBtG.gb6r/wHYe.dw8YdBxAX/P9Day.Aq7Im', '555-999-5553', NULL, '2023-04-13 20:41:53', '2023-04-13 20:41:53', NULL),
(19, 'Juan', 'Duarte', 'juan14@test.com', 'juan14', '$2y$12$mqXwKAPwyzkW9RRRi9ku9O5HlPU02c0MWz55vx4UHbGf2dowzGhBC', '555-777-5553', NULL, '2023-04-13 20:43:01', '2023-04-13 20:43:01', NULL),
(20, 'Elizabeth', 'beltre', 'eliz@test.com', 'eliz14', '$2y$12$9yFcx2KFn1NLEHX92wxRJOkvW40DfgzJuMow1IsOVEQNZCxA4ljXa', '9990008888', NULL, '2023-04-14 01:35:16', '2023-04-14 01:35:16', NULL),
(21, 'Marco', 'Polo', 'marco@test.com', 'marco_14', '$2y$12$OPpg7z8dwwDywrRIl0eAT.TJj8w5nZYtXE6ojW89rSp0jr9jzn2vC', '2227770990', NULL, '2023-04-14 02:05:09', '2023-04-14 02:05:09', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_has_permissions`
--

CREATE TABLE `user_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncar tablas antes de insertar `user_has_permissions`
--

TRUNCATE TABLE `user_has_permissions`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_has_roles`
--

CREATE TABLE `user_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncar tablas antes de insertar `user_has_roles`
--

TRUNCATE TABLE `user_has_roles`;
--
-- Volcado de datos para la tabla `user_has_roles`
--

INSERT INTO `user_has_roles` (`role_id`, `user_id`) VALUES
(1, 5),
(2, 6),
(3, 7),
(3, 11),
(4, 1),
(4, 3),
(4, 12),
(4, 14),
(4, 17),
(4, 19),
(5, 8),
(6, 10);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`article_id`),
  ADD KEY `article_category` (`id_category`),
  ADD KEY `article_author` (`id_author`),
  ADD KEY `id_category` (`id_category`);

--
-- Indices de la tabla `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `creator_id` (`creator_id`),
  ADD KEY `creator_id_2` (`creator_id`),
  ADD KEY `image_id` (`image_id`);

--
-- Indices de la tabla `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indices de la tabla `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`comment_id`),
  ADD KEY `comment_article` (`id_article`);

--
-- Indices de la tabla `distritos_municipales`
--
ALTER TABLE `distritos_municipales`
  ADD PRIMARY KEY (`distrito_id`),
  ADD KEY `municipio_id` (`municipio_id`);

--
-- Indices de la tabla `estado_civil`
--
ALTER TABLE `estado_civil`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `municipios`
--
ALTER TABLE `municipios`
  ADD PRIMARY KEY (`municipio_id`),
  ADD KEY `provincia_id` (`provincia_id`);

--
-- Indices de la tabla `newsletters`
--
ALTER TABLE `newsletters`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indices de la tabla `paises`
--
ALTER TABLE `paises`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `post_tag`
--
ALTER TABLE `post_tag`
  ADD PRIMARY KEY (`post_id`,`tag_id`);

--
-- Indices de la tabla `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indices de la tabla `provincias`
--
ALTER TABLE `provincias`
  ADD PRIMARY KEY (`provincia_id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indices de la tabla `sexos`
--
ALTER TABLE `sexos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `phone` (`phone`);

--
-- Indices de la tabla `user_has_permissions`
--
ALTER TABLE `user_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`user_id`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`user_id`);

--
-- Indices de la tabla `user_has_roles`
--
ALTER TABLE `user_has_roles`
  ADD PRIMARY KEY (`role_id`,`user_id`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`user_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `articles`
--
ALTER TABLE `articles`
  MODIFY `article_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT de la tabla `banners`
--
ALTER TABLE `banners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `comments`
--
ALTER TABLE `comments`
  MODIFY `comment_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `distritos_municipales`
--
ALTER TABLE `distritos_municipales`
  MODIFY `distrito_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=229;

--
-- AUTO_INCREMENT de la tabla `estado_civil`
--
ALTER TABLE `estado_civil`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `municipios`
--
ALTER TABLE `municipios`
  MODIFY `municipio_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=156;

--
-- AUTO_INCREMENT de la tabla `newsletters`
--
ALTER TABLE `newsletters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `paises`
--
ALTER TABLE `paises`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=895;

--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `provincias`
--
ALTER TABLE `provincias`
  MODIFY `provincia_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `sexos`
--
ALTER TABLE `sexos`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tags`
--
ALTER TABLE `tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `articles_ibfk_1` FOREIGN KEY (`id_category`) REFERENCES `categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`id_article`) REFERENCES `articles` (`article_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `profiles`
--
ALTER TABLE `profiles`
  ADD CONSTRAINT `profiles_ibfk_1` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_ibfk_1` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_has_permissions_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `user_has_permissions`
--
ALTER TABLE `user_has_permissions`
  ADD CONSTRAINT `user_has_permissions_ibfk_1` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_has_permissions_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `user_has_roles`
--
ALTER TABLE `user_has_roles`
  ADD CONSTRAINT `user_has_roles_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_has_roles_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

-- Metadatos para la base de datos blog
--
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
