<?php

/**
 * Description
 * @author Víctor L Morales M
 * @date 28/11/2022     
 * 
 * @description
 * ARCHIVO DE CONFIGURACION DEL SERVIDOR Y 
 */

 # Este es un arvhoovo de configuracion de muestra, se debe crear un archivo como este con el nombre de config.php en el mismo nivel

# CONSTANTES DEL SERVIDOR

define("DB_HOST", "localhost");  # Colocamos el servidor
define("DB_USER", "root");       # colocamos el usuario de mysql
define("DB_PASS", "dbpass");     # Colocamos el password de mysql user
define("DB_NAME", "dbname");     # colocamos el nombre de la base de datos
