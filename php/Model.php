<?php

/**
 * Description
 * @author Víctor L Morales M
 * @date 10/07/2022 
 * 
 * @description
 * CLASE PARA LA CONEXIÓN A LA BASE DE DATOS Y MÉTODOS GENERALES DEL SISTEMA
 */

// namespace core;

require_once "config.php";

class Model extends PDO
{
   private string $db_host;
   private string $db_user;
   private string $db_name;
   private string $db_pass;

   # MÉTODO CONSTRUCTOR PARA INICIAR LA CONEXIÓN A LA BASE DE DATOS AUTOMÁTICAMENTE
   # ==============================================================================
   public function __construct()
   {
      try {
         parent::__construct('mysql:host=' . $this->db_host = DB_HOST . '; dbname=' . $this->db_name = DB_NAME . '; charset=utf8', $this->db_user = DB_USER, $this->db_pass = DB_PASS);
         parent::setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      } catch (PDOException $ex) {
         $message = <<<_END
            <div style="
            box-sizing: content-box;
            width: 95%;
            margin: auto;
            padding: 20px;
            border: none;
            font: normal 23px/1 'Times New Roman', Times, serif;
            color: rgba(0,0,0,1);
            text-align: center;
            -o-text-overflow: ellipsis;
            text-overflow: ellipsis;
            background: #f9f9f9;
            text-shadow: 1px 1px 1px rgba(0,0,0,0.2) ;">                
                Error: {$ex->getMessage()}
                
            </div>
            _END;

         die($message);
      }
   }

   # ====================================================
   #
   #                * MÉTODOS PÚBLICOS *
   #
   # ====================================================

   # MÉTODO PARA DESENCRIPTAR CÓDIGO SECRETO
   # =======================================
   public function Decryption(string $string, string $hash): bool
   {
      return password_verify($string, $hash);
   }

   # MÉTODO PARA ENCRESTAR CÓDIGO SEGURO
   # ===================================
   public function Encryption($string): string
   {
      $output = password_hash($string, PASSWORD_DEFAULT);
      return $output;
   }

   # MÉTODO PARA LIMPIAR LOS INPUTS GENERALES DE LOS FORMULARIOS
   # ===========================================================
   public function Clear_string($string): string
   {
      $caracteres = ["]", "[", "==", "^", "--", "<script>", "</script>", "<script src=>", "</script type>", "SELECT * FROM", "DELETE FROM", "INSERT INTO", "{", "}"];
      $str = trim($string);
      $str = stripslashes($string);
      $str = str_ireplace($caracteres, "", $string);
      return $str;
   }

   # MÉTODO PARA HACER CONSULTA SIMPLE
   # =================================
   public function Simple_query(string $data): PDOStatement|false
   {
      $sql = $this->prepare($data);
      $sql->execute();
      return $sql;
   } 
}
