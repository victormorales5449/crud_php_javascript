<?php

require_once "Users.php";

$user = new Users;

if ($_SERVER['REQUEST_METHOD'] == "GET") {

    if ($_GET["user"] == "getUser") {
        echo json_encode($user->GetUser());
        exit;
    }

    if ($_GET["user"] == "del") {
        echo json_encode($user->DelUser($user->Clear_string($_GET["id"])));
        exit;
    }

    if ($_GET["user"] == "edit") {
        echo json_encode($user->GetDataUser($user->Clear_string($_GET["id"])));
        exit;
    }
}

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    if (isset($_POST["form_id"]) and $_POST["form_id"] =="mod") {
        echo json_encode($user->UpdateUser([
            "id" => intval($user->Clear_string($_POST["user_id"])),
            "first_name" => $user->Clear_string($_POST["first_name"]),
            "last_name" => $user->Clear_string($_POST["last_name"]),
            "username" => $user->Clear_string($_POST["username"]),
            "phone" => $user->Clear_string($_POST["phone"]),
            "email" => $user->Clear_string($_POST["email"]),
        ]));
        exit;
    }

    # Deben crear las validaaciones para que no reciban datos vacios
    if (isset($_POST["form_id"]) and $_POST["form_id"] =="add") {
        echo json_encode($user->SetUser([
            "first_name" => $user->Clear_string($_POST["first_name"]),
            "last_name" => $user->Clear_string($_POST["last_name"]),
            "username" => $user->Clear_string($_POST["username"]),
            "phone" => $user->Clear_string($_POST["phone"]),
            "email" => $user->Clear_string($_POST["email"]),
            "password" => $user->Encryption(123456),
        ]));
        exit;
    }
}