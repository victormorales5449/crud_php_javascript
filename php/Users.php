<?php

require_once "Model.php";

class Users extends Model
{
    public function GetUser()
    {
        $stmSQL = $this->Simple_query("SELECT * FROM `users`");
        return $stmSQL->fetchAll(PDO::FETCH_ASSOC);
    }

    public function DelUser(int $id)
    {
        try {
            $this->beginTransaction();

            $sql = "DELETE FROM users WHERE id = ?";
            $stmSQL = $this->prepare($sql);
            $stmSQL->bindParam(1, $id);
            $stmSQL->execute();

            $this->commit();

            return [1 => "Usuario eliminado.", "User" => $this->GetUser()];
        } catch (PDOException $th) {
            $this->rollback();
            return ["Error" => $th->getMessage()];
        }
    }

    public function GetDataUser(int $id)
    {
        $stmSQL = $this->Simple_query("SELECT * FROM `users` WHERE `id`= {$id};");
        return $stmSQL->fetchAll(PDO::FETCH_ASSOC);
    }

    public function GetInfoUser(string $username)
    {
        $stmSQL = $this->Simple_query("SELECT * FROM `users` WHERE `username`= '{$username}';");
        return $stmSQL->fetchAll(PDO::FETCH_ASSOC);
    }

    public function SetUser(array $user)
    {
        try {
            $this->beginTransaction();

            $userExist = $this->GetInfoUser($user["username"]);

            if (!empty($userExist)) {
                throw new PDOException("Este usuario ya existe.");
            }

            $sql = "INSERT INTO `users`(`first_name`, `last_name`, `email`, `username`,`password`, `phone`) VALUES (?,?,?,?,?,?);";
            $stmSQL = $this->prepare($sql);
            $stmSQL->bindParam(1, $user["first_name"]);
            $stmSQL->bindParam(2, $user["last_name"]);
            $stmSQL->bindParam(3, $user["email"]);
            $stmSQL->bindParam(4, $user["username"]);
            $stmSQL->bindParam(5, $user["password"]);
            $stmSQL->bindParam(6, $user["phone"]);
            $stmSQL->execute();

            $this->commit();
            return [1 => "Usuario registrado.", "User" => $this->GetUser()];
        } catch (PDOException $th) {
            $this->rollback();
            return ["Error" => $th->getMessage()];
        }
    }

    public function UpdateUser(array $user)
    {
        try {
            $this->beginTransaction();

            $sql = "UPDATE `users` SET `first_name`= ?,`last_name`= ?,`email`= ?,`username`= ?,`phone`= ? WHERE `id`= ?;";
            $stmSQL = $this->prepare($sql);
            $stmSQL->bindParam(1, $user["first_name"]);
            $stmSQL->bindParam(2, $user["last_name"]);
            $stmSQL->bindParam(3, $user["email"]);
            $stmSQL->bindParam(4, $user["username"]);
            $stmSQL->bindParam(5, $user["phone"]);
            $stmSQL->bindParam(6, $user["id"]);
            $stmSQL->execute();

            $this->commit();
            return [1 => "Usuario actualizado.", "User" => $this->GetUser()];
        } catch (PDOException $th) {
            $this->rollback();
            return ["Error" => $th->getMessage()];
        }
    }
}
