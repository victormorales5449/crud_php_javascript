// CONSTANTES PARA LA SELECCIÓN DE ELEMENTOS
// =========================================
export const SelectId = (x) => document.getElementById(x);
export const Is_SelectId = (x) => !!document.getElementById(x);
export const Is_Select = (x) => !!document.querySelector(x);

export const Select = (x, y = false) => {
    let el = x.trim()
    if (y) {
        return [...document.querySelectorAll(el)];
    }
    return document.querySelector(el);
};

// FUNCIÓN PARA CAMBIAR EL IDIOMA ESPAÑOL DE LOS DATATABLE
// =======================================================
const idioma_espanol = {
    sProcessing: "Procesando...",
    sLengthMenu: "Mostrar _MENU_ registros",
    sZeroRecords: "No se encontraron resultados",
    sEmptyTable: "Ningún dato disponible en esta tabla",
    sInfo:
        "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    sInfoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
    sInfoFiltered: "(filtrado de un total de _MAX_ registros)",
    sInfoPostFix: "",
    sSearch: "Buscar:",
    sUrl: "",
    sInfoThousands: ",",
    sLoadingRecords: "Cargando...",
    oPaginate: {
        sFirst: "Primero",
        sLast: "Último",
        sNext: "Siguiente",
        sPrevious: "Anterior",
    },
    oAria: {
        sSortAscending: ": Activar para ordenar la columna de manera ascendente",
        sSortDescending: ": Activar para ordenar la columna de manera descendente",
    },
};

// AÑADIR UN OBJETO DE ATRIBUTOS A UN ELEMENTO
// ===========================================
const addAttributes = (element, attrObj) => {
    for (let attr in attrObj) {
        if (attrObj.hasOwnProperty(attr)) element.setAttribute(attr, attrObj[attr]);
    }
};

// CREAR ELEMENTOS CON ATRIBUTOS E HIJOS
// =====================================
export const createCustomElement = (element, attributes, children) => {
    let customElement = document.createElement(element);
    if (children !== undefined)
        children.forEach((el) => {
            if (el.nodeType) {
                if (el.nodeType === 1 || el.nodeType === 11)
                    customElement.appendChild(el);
            } else {
                customElement.innerHTML += el;
            }
        });
    addAttributes(customElement, attributes);
    return customElement;
};

// CLASE PRINCIPAL DEL SISTEMA
// ===========================
export default class Crud {
    constructor() {
        this.VERSION = "1.0";
        this.AUTHOR = "Víctor L Morales M";
        this.SUPPORT = "suport@Sigep.com";
    }

    // MÉTODO PARA CALCULAR LA EDAD
    // ============================
    initEdad(date) {
        // const fecha = SelectId("nfecha"),
        // result = SelectId("edad");
        if (Is_SelectId(date.inputFecha)) {
            const fecha = SelectId(date.inputFecha),
                result = SelectId(date.inputEdad);
            fecha.addEventListener("blur", () => {
                const hoy = new Date();
                let cumpleanos = new Date(fecha.value);
                let edad = hoy.getFullYear() - cumpleanos.getFullYear();
                let m = hoy.getMonth() - cumpleanos.getMonth();

                if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
                    edad--;
                }

                result.value = parseInt(edad);
            });
        }
    }

    // MÉTODO PARA CREAR EL MODAL MESSAGE
    // ==================================
    initModal(info) {
        const icon = createCustomElement("i", {
            class: info.icon,
        }, []);

        const titleModal = createCustomElement("h4", {
            class: "modal-title",
        }, [icon, info.title]);

        const closeModal = createCustomElement("button", {
            id: "close",
            type: "button",
            class: "btn-close",
        }, []);

        const headerModal = createCustomElement("div", {
            class: "modal-header",
        }, [titleModal, closeModal]);

        const bodyModal = createCustomElement("div", {
            id: "modal-body",
            class: "modal-body",
        }, []);

        const botonYesModal = createCustomElement("button", {
            id: "btnOK",
            type: "button",
            class: "btn btn-danger",
        }, ["Si"]);

        const botonNoModal = createCustomElement("button", {
            id: "btnNo",
            type: "button",
            class: "btn btn-success",
        }, ["No"]);

        const footerModal = createCustomElement("div", {
            class: "modal-footer",
        }, [botonYesModal, botonNoModal]);

        const modalContentEl = createCustomElement("div", {
            id: "modal-content",
            class: "modal-content",
        }, [headerModal, bodyModal, footerModal]);

        const modalDialog = createCustomElement("div", {
            class: "modal-dialog",
        }, [modalContentEl]);

        const modalContainerEl = createCustomElement("div", {
            id: "M-container",
            class: "modal fade",
            role: "dialog",
        }, [modalDialog]);

        const divBackDrop = createCustomElement("div", {
            class: "modal-backdrop fade show",
            id: "mb",
        }, []);

        // AGREGAMOS LOS ELEMENTOS 
        document.body.appendChild(modalContainerEl);
        document.body.appendChild(divBackDrop);

        SelectId("M-container").style.overflow = "auto";

        if (info.formId !== undefined) {
            const FormModal = createCustomElement("form", {
                id: info.formId
            }, []);

            SelectId("modal-body").appendChild(FormModal);

            SelectId("btnNo").setAttribute("id", "btnCancelar");
            SelectId("btnCancelar").textContent = "Cancelar";
            SelectId("btnCancelar").classList.remove("btn-success");
            SelectId("btnCancelar").classList.add("btn-danger");

            SelectId("btnOK").setAttribute("id", "btnSave");
            SelectId("btnSave").textContent = "Guardar";
            SelectId("btnSave").classList.remove("btn-danger");
            SelectId("btnSave").classList.add("btn-success");
        }

        if (info.message !== undefined) {
            const parrafoModal = createCustomElement("p", { id: "salir", class: "text-center" }, [info.message]);
            SelectId("modal-body").appendChild(parrafoModal);
        }

        // INICIALIZAMOS EL MODAL
        SelectId("M-container").classList.add("show");
        SelectId("M-container").style.display = "block";

        // REMOVER EL MODAL
        const removeModal = () => {
            document.body.removeChild(divBackDrop);
            document.body.removeChild(modalContainerEl);
        };

        // CERRAR EN MODAL CON UN CLICK
        modalContainerEl.addEventListener("click", (e) => {
            if (e.target === modalContainerEl) removeModal();
        });

        // CERRAR EL MODAL CON ESC
        const offCloseModalEsc = () => removeEventListener("keyup", closeModalEsc);


        // FUNCIÓN PARA CERRAR CON LA TECLA ESC
        const closeModalEsc = (e) => {
            if (e.key === "Escape") {
                offCloseModalEsc();
                removeModal();
            }
        };

        addEventListener("keyup", closeModalEsc);

        const btn = SelectId("close");

        btn.addEventListener("click", () => {
            removeModal();
        });
    }

    // MÉTODO PARA PREVISUALIZAR LA IMAGEN ANTES DE SUBIRLA
    // ====================================================
    initPreviewImage() {
        if (Is_SelectId("image")) {
            SelectId("image").addEventListener("change", async (e) => {
                const fileInput = e.target; // File List Object
                const fileExtension = /(.jpg|.jpeg|.png|.gif)$/i; // extenciones admitidas

                if (!fileExtension.exec(fileInput.value)) {
                    SelectId("Message").innerHTML = `Favor subir un archivo que sea de tipo imagen`;
                    setTimeout(() => {
                        SelectId("Message").innerHTML = "";
                    }, 6000);
                    return false;
                } else {
                    if (fileInput.files && fileInput.files[0]) {
                        const visor = new FileReader(); // lector de archivos
                        visor.addEventListener("load", (e) => {
                            SelectId("foto").src = `${e.target.result}`;
                            SelectId("foto").title = `${fileInput.files[0].name}`;
                            SelectId("foto").alt = `${fileInput.files[0].name}`;
                        });
                        visor.readAsDataURL(fileInput.files[0]);
                    }
                }
            });
        }
    }

    // MÉTODO PARA CALCULAR Y MOSTRAR EL PESO DEL ARCHIVO
    // ==================================================
    formatBytes(bytes, decimals = 2) {
        if (bytes === 0) return '0 Bytes';

        const k = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

        const i = Math.floor(Math.log(bytes) / Math.log(k));

        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }

    // FUNCIÓN PARA CREAR INPUTS PERSONALIZADOS
    // ========================================
    initInputs(Element, data, type = "text") {
        if (type == "hidden") {
            const idNota = createCustomElement("input", {
                type: type,
                id: data.name,
                name: data.name,
                value: data.value !== undefined ? data.value : "",
            }, []);
            SelectId(Element).appendChild(idNota);
            return;
        }

        const input = createCustomElement("input", {
            class: "form-control",
            type: type,
            id: data.name,
            name: data.name,
            placeholder: data.placeholder !== undefined ? data.placeholder : data.title,
        });

        const label = createCustomElement("label", {
            for: data.name,
        }, [data.title + ":"]);

        const div1 = createCustomElement("div", {
            class: "form-group",
        }, [label, input]);

        const div2 = createCustomElement("div", {
            class: data.size,
        }, [div1]);

        SelectId(Element).appendChild(div2);

        if (data.value !== undefined && data.value !== null) {
            SelectId(data.name).setAttribute("value", data.value)
        }

        if (data.function !== undefined) {
            SelectId(data.name).setAttribute("onkeypress", data.function)
        }

        if (data.center !== undefined) {
            SelectId(data.name).classList.add(data.center)
        }

        if (data.maxlength !== undefined) {
            SelectId(data.name).setAttribute("maxlength", data.maxlength)
        }
    }

    // MÉTODO PARA CREAR SELECT PERSONALIZADOS
    // ========================================
    initSelects(Element, info) {
        const select = createCustomElement("select", {
            class: "form-control",
            id: info.id,
            name: info.name,
            style: info.width,
            ["data-select2bs4"]: ""
        }, [info.data]);

        const label = createCustomElement("label", {
            for: info.id,
        }, [`${info.title}:`]);

        const div1 = createCustomElement("div", {
            class: "form-group",
        }, [label, select]);

        const div2 = createCustomElement("div", {
            class: info.size,
        }, [div1]);

        Element.appendChild(div2);
    }

    // MÉTODO PARA CREAR TABLA PERSONALIZADA
    // ======================================
    table(element, data, name) {
        const tr = createCustomElement("tr", {}, [data.th]);
        const thead = createCustomElement("thead", {}, [tr]);
        const tbody = createCustomElement("tbody", {}, [data.body]);
        const table = createCustomElement("table", {
            id: name,
            class: "table table-bordered table-striped dataTable"
        }, [thead, tbody]);

        SelectId(element).appendChild(table);
    }

    // MÉTODO PARA COLOCAR LA FECHA EN FORMATO DOMINICANO
    // ==================================================
    formatDate(date) {
        const fecha = new Date(date);
        let dia, mes;
        let d = fecha.getDate() + 1;
        let m = fecha.getMonth() + 1;
        let anio = fecha.getFullYear();

        dia = d < 10 ? dia = `0${d}` : dia = d;
        mes = m < 10 ? mes = `0${m}` : mes = m;

        return `${dia}-${mes}-${anio}`;
    }

    // MÉTODO PARA INDICAR EL ESTADO 
    // =============================
    Status(valor) {
        // return valor == 1 ? "Activo" : "Inactivo";
        return valor == 1
            ? `<span class="badge badge-success">Activo</span>`
            : `<span class="badge badge-danger">Inactivo</span>`;
    }

    // MÉTODO PRA CREAR TABLA DINÁMICAMENTE
    // ====================================
    initDataTable(table, params) {
        $(table).DataTable({
            responsive: true,
            columnDefs: [
                {
                    targets: [0],
                    visible: true,
                    serchable: false,
                },
            ],
            lengthChange: params.len !== undefined ? params.len : true,
            paging: params.paging !== undefined ? params.paging : true,
            searching: params.search !== undefined ? params.search : true,
            autoWidth: params.width !== undefined ? params.width : false,
            info: params.info !== undefined ? params.info : true,
            language: idioma_espanol,
            lengthMenu: [
                [10, 15, 25, 50, -1],
                [10, 15, 25, 50, "All"]],
            iDisplayLength: params.le !== undefined ? params.le : 10,
            buttons: params.buttons !== undefined ? '' : ["excel", "pdf", "print"]
        }).buttons().container().appendTo(`${table}_wrapper .col-md-6:eq(0)`);
    }

    // MÉTODO PARA CERRAR EL MODAL
    // ===========================
    InitCloseModal() {
        SelectId("mb").remove();
        SelectId("M-container").remove();
    }

    initSVG() {
        const path1 = createCustomElement("path", {
            "data-toggle": "modal",
            d: "M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"
        }, []);

        const path2 = createCustomElement("path", {
            "data-toggle": "modal",
            d: "M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"
        }, []);

        const path3 = createCustomElement("path", {
            "data-toggle": "modal",
            d: "M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"
        }, []);

        const symbol1 = createCustomElement("symbol", {
            id: "check-circle-fill",
            viewBox: "0 0 16 16",
        }, [path1]);

        const symbol2 = createCustomElement("symbol", {
            id: "info-fill",
            viewBox: "0 0 16 16",
        }, [path2]);

        const symbol3 = createCustomElement("symbol", {
            id: "exclamation-triangle-fill",
            viewBox: "0 0 16 16",
        }, [path3]);

        const svg = createCustomElement("svg", {
            xmlns: "http://www.w3.org/2000/svg",
            style: "display: none;",
        }, [symbol1, symbol2, symbol3]);

        Select("body").insertBefore(svg, Select(".container-fluid"));
    }

    initMessageAlert(data) {
        let label, type;

        switch (data.type) {
            case "primary":
                label = "Info:";
                type = "#info-fill";
                break;

            case "success":
                label = "Success:";
                type = "#check-circle-fill";
                break;

            case "warning":
                label = "Warning:";
                type = "#exclamation-triangle-fill";
                break;

            case "danger":
                label = "Danger:";
                type = "#exclamation-triangle-fill";
                break;

            default:
                label = "Info:";
                type = "#info-fill";
                break;
        }

        const use = createCustomElement("use", {
            ["xlink:href"]: type
        }, []);

        const svg = createCustomElement("svg", {
            class: "bi flex-shrink-0 me-2",
            role: "img",
            ["aria-label"]: label,
        }, [use]);

        const message = createCustomElement("div", {}, [data.message]);

        const alert = createCustomElement("div", {
            id: "Alert",
            class: `alert alert-${data.type} d-flex align-items-center`,
            role: "alert"
        }, [svg, message]);

        if (data.element !== undefined) {
            SelectId("form-user").appendChild(alert);            
        } else{
            Select("body").appendChild(alert);
            SelectId("Alert").classList.add("alerts")
        }

    }
}
