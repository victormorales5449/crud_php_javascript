import Crud, { SelectId, Select, createCustomElement } from "./crud.js";

class Main extends Crud {
    async LoadData() {
        try {
            const reqUser = await fetch("php/petition.php?user=getUser");
            const resUser = await reqUser.json();

            this.tableCrud([resUser, "List-crud"]);

        } catch (error) {
            console.error(error);
        }
    }

    async deleteUser(id) {
        try {
            const reqDelUser = await fetch(`php/petition.php?user=del&id=${id}`);
            const resDelUser = await reqDelUser.json();

            if (resDelUser["Error"]) {
                throw resDelUser["Error"];
            }

            this.InitCloseModal();

            this.initMessageAlert({
                message: resDelUser[1],
                type: "success"
            });

            setTimeout(() => {
                SelectId("Alert").remove();
            }, 5000);

            this.tableCrud([resDelUser.User, "List-crud"]);

        } catch (error) {
            console.error(error);
            this.initMessageAlert({
                message: error,
                type: "danger"
            });

            setTimeout(() => {
                SelectId("Alert").remove();
            }, 5000);
        }
    }

    async GetUser(id) {
        try {
            const reqEditUser = await fetch(`php/petition.php?user=edit&id=${id}`);
            const resEditUser = await reqEditUser.json();

            if (resEditUser["Error"]) {
                throw resEditUser["Error"];
            }

            this.formUser();

            SelectId("form_id").setAttribute("value", "mod");
            SelectId("user_id").setAttribute("value", resEditUser[0].id);
            SelectId("first_name").setAttribute("value", resEditUser[0].first_name);
            SelectId("last_name").setAttribute("value", resEditUser[0].last_name);
            SelectId("username").setAttribute("value", resEditUser[0].username);
            SelectId("phone").setAttribute("value", resEditUser[0].phone);
            SelectId("email").setAttribute("value", resEditUser[0].email);

        } catch (error) {
            console.error(error);
            this.initMessageAlert({
                message: error,
                type: "danger"
            });

            setTimeout(() => {
                SelectId("Alert").remove();
            }, 5000);
        }
    }

    formUser() {
        this.initInputs("form-user", {
            name: "user_id",
        }, "hidden");

        this.initInputs("form-user", {
            name: "form_id",
        }, "hidden");

        const row1 = createCustomElement("div", {
            class: "row mb-3",
            id: "row1"
        }, [])

        SelectId("form-user").appendChild(row1);

        this.initInputs("row1", {
            name: "first_name",
            placeholder: "Ingrese su nombre",
            title: "Nombre",
            size: "col-6"
        });

        this.initInputs("row1", {
            name: "last_name",
            placeholder: "Ingrese su apellido",
            title: "Apellido",
            size: "col-6"
        });

        const row2 = createCustomElement("div", {
            class: "row mb-3",
            id: "row2"
        }, [])

        SelectId("form-user").appendChild(row2);

        this.initInputs("row2", {
            name: "username",
            placeholder: "Ingrese el username",
            title: "Username",
            size: "col-6"
        });

        this.initInputs("row2", {
            name: "phone",
            placeholder: "Ingrese su Teléfono",
            title: "Teléfono",
            size: "col-6"
        });

        const row3 = createCustomElement("div", {
            class: "row mb-3",
            id: "row3"
        }, [])

        SelectId("form-user").appendChild(row3);

        this.initInputs("row3", {
            name: "email",
            placeholder: "Ingrese su Correo electrónico",
            title: "E-mail",
            size: "col-12"
        });
    }

    async saveUser() {
        try {
            const form = new FormData(SelectId("form-user"));
            const reqUpdateUser = await fetch("php/petition.php", {
                method: "POST",
                body: form
            });

            const resUpdateUser = await reqUpdateUser.json();

            if (resUpdateUser["Error"]) {
                throw resUpdateUser["Error"];
            }

            this.InitCloseModal();

            this.initMessageAlert({
                message: resUpdateUser[1],
                type: "success"
            });

            setTimeout(() => {
                SelectId("Alert").remove();
            }, 5000);

            this.tableCrud([resUpdateUser.User, "List-crud"]);

        } catch (error) {
            console.error(error);
            this.initMessageAlert({
                message: error,
                type: "danger",
                element: "form-user"
            });

            setTimeout(() => {
                SelectId("Alert").remove();
            }, 5000);
        }
    }

    tableCrud(ElementData) {
        let tbody = "";
        let num = 1;
        ElementData[0].forEach((data) => {
            tbody += `<tr>`;
            tbody += `<td class="text-center">${num}</td>`;
            tbody += `<td class="text-center">${data.first_name}</td>`;
            tbody += `<td class="text-center">${data.last_name}</td>`;
            tbody += `<td class="text-center">${data.phone}</td>`;
            tbody += `<td class="text-center">${data.email}</td>`;
            tbody += `<td class="text-center">
                <button title="Modificar" class="btn btn-info btn-sm btn-edit" data-edit="${data.id}">
                    <i class="fa fa-edit"></i></button>

                <button title="Eliminar" class="btn btn-danger btn-sm btn-del" data-del="${data.id}">
                    <i class="fas fa-trash"></i></button>
            </td>`;
            tbody += `</tr>`;

            num++
        });

        $('#crud').DataTable().destroy();
        SelectId(ElementData[1]).innerHTML = tbody;
        this.initDataTable(`#crud`, [{ width: false }]);

    }

    initLoadData() {
        addEventListener("DOMContentLoaded", evnt => {
            this.LoadData();
        });

        document.addEventListener("click", evnt => {
            const element = evnt.target;

            if (element.matches(".btn-edit")) {
                const id = element.dataset.edit;

                this.initModal({
                    title: " Editar",
                    icon: "fa fa-edit",
                    formId: "form-user"
                });

                this.GetUser(id);
            }

            if (element.matches(".btn-del")) {
                const id = element.dataset.del;

                this.initModal({
                    title: " Eliminar",
                    icon: "fas fa-trash",
                    message: "¿Estas seguro que quieres eliminar este usuario?"
                });

                SelectId("btnOK").addEventListener("click", () => {
                    this.deleteUser(id);
                });

                SelectId("btnNo").addEventListener("click", () => {
                    this.InitCloseModal();
                });
            }

            if (element.matches("#btnCancelar")) {
                this.InitCloseModal();
            }

            if (element.matches("#btn-reg")) {
                this.initModal({
                    title: " Agregar",
                    icon: "fa fa-plus",
                    formId: "form-user"
                });

                this.formUser();

                SelectId("form_id").setAttribute("value", "add");
            }

            if (element.matches("#btnSave")) {
                // No voy a colocar validaciones, le corresponde a usted
                this.saveUser();
            }
        })
    }
}

const crudjs = new Main();
crudjs.initLoadData();
crudjs.initSVG();
